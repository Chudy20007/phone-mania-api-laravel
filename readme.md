<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Introduction

Phone Mania API is an application based on the framework Laravel. This API allows you to manage smartphones shop/wiki.

## Features

*  CRUD for the all controllers with data validation (requests, validators)
*  Full authorization with authentication (validators, policies, security middlewares)
*  Security against: CORS, CSRF , XSS, SQL Injection, Bots
*  Authorization type:  Authorization Bearer
*  User interface: recovering and resetting password, account verification

## Used Components

* Middleware : CheckRole
* Requests
* Policies
* Validators
* Jobs
* Mail
* Notifications
* Enities
* Migrations
* Seeders
* Commands (delete expired tokens from verify users table)

## Used packages

* [CORS Middleware for Laravel 5 - MIT](https://github.com/barryvdh/laravel-cors)
* [Laravel 5 Repositories - MIT](https://github.com/andersao/l5-repository)


## Installation

1.  Clone the project: `git clone https://github.com/Chudy20007/phone-mania-api-laravel`.
2.  In the CLI (composer) pass this command: `composer install `. 
3.  Generate  JWT public and private key: `openssl genrsa -passout pass:secret -out private.pem -aes256 4096`, `openssl genrsa -passin pass:secret -pubout -in private.pem -out public.pem`
3.  Create a database and configure **.env** file (details in the next subsection).
4.  Run the migrations with seeders (important! don't change the values 'names' in the 'roles' table): `php artisan migrate:fresh` , `php artisan db:seed`.
5.  Run application: `php artisan:serve`.

## Configuration

*You must specify environment variables in the **.env** file.*

Example:
1. APP_URL=http://phonemania.pl/api - specify Frontend APP URL
2. MAIL_DRIVER=smtp
3. MAIL_HOST=smtp.gmail.com
4. MAIL_PORT=587
5. MAIL_USERNAME=youremail@gmail.com - specify APP email for the user interface (mails, notifications)
6. MAIL_PASSWORD=your gmail app password
7. MAIL_ENCRYPTION=tls
3. FILES_PATH=C:\xampp\htdocs\phone_mania_api\storage\img\ - specify path for the uploaded files
4. FILES_URL = http://phonemania.pl/api/storage/app/public/img/ - specify url for the uploaded smartphones images
5. JWT_PUBLIC_KEY=jwt/public.pem - generate jwt public key with OpenSSH (**remember => generate key to the public.pem file!**)
6. JWT_PRIVATE_KEY=jwt/private.pem - generate jwt private key with OpenSSH (**remember => generate key to the private.pem file!**)
7. JWT_PASSPHRASE - specify jwt passphrase

If you have problem with configure .env file you can check **.env-example** file.

**Generate jwt secret key**

Set the JWTAuth secret key used to sign the tokens: `php artisan jwt:secret`
This will update your .env file with something like JWT_SECRET=foobar

For more information check the package docs: [text](https://jwt-auth.readthedocs.io/en/develop/laravel-installation/)


## Available addresses in the API

<table style='width:100%'>
    <tr>
        <td width='10%'>
            <h4>HTTP Method</h4>
        </td>
        <td width='10%'>
            <h4>Route</h4>
        </td>
        <td width='80%'>
            <h4>Corresponding Action</h4>
        </td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/user-orders</td>
        <td>App\Http\Controllers\OrdersController@userOrders</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/rates</td>
        <td>App\Http\Controllers\RatesController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/authenticate/refresh</td>
        <td>App\Http\Controllers\AuthenticateController@refreshToken</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/orders</td>
        <td>App\Http\Controllers\OrdersController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/orders/create</td>
        <td>App\Http\Controllers\OrdersController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/orders</td>
        <td>App\Http\Controllers\OrdersController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/orders/{order}</td>
        <td>App\Http\Controllers\OrdersController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/orders/{order}/edit</td>
        <td>App\Http\Controllers\OrdersController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/orders/{order}</td>
        <td>App\Http\Controllers\OrdersController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/orders/{order}</td>
        <td>App\Http\Controllers\OrdersController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rates</td>
        <td>App\Http\Controllers\RatesController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rates/create</td>
        <td>App\Http\Controllers\RatesController@create</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rates/{rate}</td>
        <td>App\Http\Controllers\RatesController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rates/{rate}/edit</td>
        <td>App\Http\Controllers\RatesController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/rates/{rate}</td>
        <td>App\Http\Controllers\RatesController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/rates/{rate}</td>
        <td>App\Http\Controllers\RatesController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/smartphones/create</td>
        <td>App\Http\Controllers\SmartphonesController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/smartphones</td>
        <td>App\Http\Controllers\SmartphonesController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/smartphones/{smartphone}/edit</td>
        <td>App\Http\Controllers\SmartphonesController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/smartphones/{smartphone}</td>
        <td>App\Http\Controllers\SmartphonesController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/smartphones/{smartphone}</td>
        <td>App\Http\Controllers\SmartphonesController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/manufacturers/create</td>
        <td>App\Http\Controllers\ManufacturersController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/manufacturers</td>
        <td>App\Http\Controllers\ManufacturersController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/manufacturers/{manufacturer}</td>
        <td>App\Http\Controllers\ManufacturersController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/manufacturers/{manufacturer}/edit</td>
        <td>App\Http\Controllers\ManufacturersController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/manufacturers/{manufacturer}</td>
        <td>App\Http\Controllers\ManufacturersController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/manufacturers/{manufacturer}</td>
        <td>App\Http\Controllers\ManufacturersController@destroy</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/orders/{id}/update-status</td>
        <td>App\Http\Controllers\OrdersController@updateOrderStatus</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/users</td>
        <td>App\Http\Controllers\UsersController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/users/create</td>
        <td>App\Http\Controllers\UsersController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/users</td>
        <td>App\Http\Controllers\UsersController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/users/{user}</td>
        <td>App\Http\Controllers\UsersController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/users/{user}/edit</td>
        <td>App\Http\Controllers\UsersController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/users/{user}</td>
        <td>App\Http\Controllers\UsersController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/users/{user}</td>
        <td>App\Http\Controllers\UsersController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/roles</td>
        <td>App\Http\Controllers\RolesController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/roles/create</td>
        <td>App\Http\Controllers\RolesController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/roles</td>
        <td>App\Http\Controllers\RolesController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/roles/{role}</td>
        <td>App\Http\Controllers\RolesController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/roles/{role}/edit</td>
        <td>App\Http\Controllers\RolesController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/roles/{role}</td>
        <td>App\Http\Controllers\RolesController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/roles/{role}</td>
        <td>App\Http\Controllers\RolesController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/memories</td>
        <td>App\Http\Controllers\MemoriesController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/memories/create</td>
        <td>App\Http\Controllers\MemoriesController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/memories</td>
        <td>App\Http\Controllers\MemoriesController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/memories/{memory}</td>
        <td>App\Http\Controllers\MemoriesController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/memories/{memory}/edit</td>
        <td>App\Http\Controllers\MemoriesController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/memories/{memory}</td>
        <td>App\Http\Controllers\MemoriesController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/memories/{memory}</td>
        <td>App\Http\Controllers\MemoriesController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/tags</td>
        <td>App\Http\Controllers\TagsController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/tags/create</td>
        <td>App\Http\Controllers\TagsController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/tags</td>
        <td>App\Http\Controllers\TagsController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/tags/{tag}</td>
        <td>App\Http\Controllers\TagsController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/tags/{tag}/edit</td>
        <td>App\Http\Controllers\TagsController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/tags/{tag}</td>
        <td>App\Http\Controllers\TagsController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/tags/{tag}</td>
        <td>App\Http\Controllers\TagsController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/cameras</td>
        <td>App\Http\Controllers\CamerasController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/cameras/create</td>
        <td>App\Http\Controllers\CamerasController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/cameras</td>
        <td>App\Http\Controllers\CamerasController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/cameras/{camera}</td>
        <td>App\Http\Controllers\CamerasController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/cameras/{camera}/edit</td>
        <td>App\Http\Controllers\CamerasController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/cameras/{camera}</td>
        <td>App\Http\Controllers\CamerasController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/cameras/{camera}</td>
        <td>App\Http\Controllers\CamerasController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rams</td>
        <td>App\Http\Controllers\RamsController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rams/create</td>
        <td>App\Http\Controllers\RamsController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/rams</td>
        <td>App\Http\Controllers\RamsController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rams/{ram}</td>
        <td>App\Http\Controllers\RamsController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/rams/{ram}/edit</td>
        <td>App\Http\Controllers\RamsController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/rams/{ram}</td>
        <td>App\Http\Controllers\RamsController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/rams/{ram}</td>
        <td>App\Http\Controllers\RamsController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/resolutions</td>
        <td>App\Http\Controllers\ResolutionsController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/resolutions/create</td>
        <td>App\Http\Controllers\ResolutionsController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/resolutions</td>
        <td>App\Http\Controllers\ResolutionsController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/resolutions/{resolution}</td>
        <td>App\Http\Controllers\ResolutionsController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/resolutions/{resolution}/edit</td>
        <td>App\Http\Controllers\ResolutionsController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/resolutions/{resolution}</td>
        <td>App\Http\Controllers\ResolutionsController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/resolutions/{resolution}</td>
        <td>App\Http\Controllers\ResolutionsController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/displays</td>
        <td>App\Http\Controllers\DisplaysController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/displays/create</td>
        <td>App\Http\Controllers\DisplaysController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/displays</td>
        <td>App\Http\Controllers\DisplaysController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/displays/{display}</td>
        <td>App\Http\Controllers\DisplaysController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/displays/{display}/edit</td>
        <td>App\Http\Controllers\DisplaysController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/displays/{display}</td>
        <td>App\Http\Controllers\DisplaysController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/displays/{display}</td>
        <td>App\Http\Controllers\DisplaysController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/batteries</td>
        <td>App\Http\Controllers\BatteriesController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/batteries/create</td>
        <td>App\Http\Controllers\BatteriesController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/batteries</td>
        <td>App\Http\Controllers\BatteriesController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/batteries/{battery}</td>
        <td>App\Http\Controllers\BatteriesController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/batteries/{battery}/edit</td>
        <td>App\Http\Controllers\BatteriesController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/batteries/{battery}</td>
        <td>App\Http\Controllers\BatteriesController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/batteries/{battery}</td>
        <td>App\Http\Controllers\BatteriesController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/processors</td>
        <td>App\Http\Controllers\ProcessorsController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/processors/create</td>
        <td>App\Http\Controllers\ProcessorsController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/processors</td>
        <td>App\Http\Controllers\ProcessorsController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/processors/{processor}</td>
        <td>App\Http\Controllers\ProcessorsController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/processors/{processor}/edit</td>
        <td>App\Http\Controllers\ProcessorsController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/processors/{processor}</td>
        <td>App\Http\Controllers\ProcessorsController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/processors/{processor}</td>
        <td>App\Http\Controllers\ProcessorsController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/systems</td>
        <td>App\Http\Controllers\SystemsController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/systems/create</td>
        <td>App\Http\Controllers\SystemsController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/systems</td>
        <td>App\Http\Controllers\SystemsController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/systems/{system}</td>
        <td>App\Http\Controllers\SystemsController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/systems/{system}/edit</td>
        <td>App\Http\Controllers\SystemsController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/systems/{system}</td>
        <td>App\Http\Controllers\SystemsController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/systems/{system}</td>
        <td>App\Http\Controllers\SystemsController@destroy</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/payments</td>
        <td>App\Http\Controllers\PaymentsController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/payments/create</td>
        <td>App\Http\Controllers\PaymentsController@create</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/payments</td>
        <td>App\Http\Controllers\PaymentsController@store</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/payments/{payment}</td>
        <td>App\Http\Controllers\PaymentsController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/payments/{payment}/edit</td>
        <td>App\Http\Controllers\PaymentsController@edit</td>
    </tr>
    <tr>
        <td>PUT</td>
        <td>api/payments/{payment}</td>
        <td>App\Http\Controllers\PaymentsController@update</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/payments/{payment}</td>
        <td>App\Http\Controllers\PaymentsController@destroy</td>
    </tr>
    <tr>
        <td>DELETE</td>
        <td>api/orders/{orderID}/delete/{productID}</td>
        <td>App\Http\Controllers\OrdersController@removeProductFromOrder</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/logout</td>
        <td>App\Http\Controllers\AuthController@logout</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/routes</td>
        <td>Closure</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/smartphones/{id}</td>
        <td>App\Http\Controllers\SmartphonesController@show</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/manufacturers</td>
        <td>App\Http\Controllers\ManufacturersController@index</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/find-products-by-manufacturers</td>
        <td>App\Http\Controllers\SmartphonesController@findProductsByManufacturers</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/smartphones</td>
        <td>App\Http\Controllers\SmartphonesController@index</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/login</td>
        <td>App\Http\Controllers\AuthController@login</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/login</td>
        <td>App\Http\Controllers\AuthController@login</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/find/{value}</td>
        <td>App\Http\Controllers\SmartphonesController@findProducts</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/storage/app/public/img/photos/{img}</td>
        <td>App\Http\Controllers\SmartphonesController@getPhotos</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/refresh</td>
        <td>App\Http\Controllers\AuthController@refresh</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/user/verify/{token}</td>
        <td>App\Http\Controllers\UsersController@verifyUser</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/reset</td>
        <td>App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/email</td>
        <td>App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>api/reset/{token}</td>
        <td>App\Http\Controllers\Auth\ResetPasswordController@showResetForm</td>
    </tr>
    <tr>
        <td>POST</td>
        <td>api/reset</td>
        <td>App\Http\Controllers\Auth\ResetPasswordController@reset</td>
    </tr>
    <tr>
        <td>GET</td>
        <td>/</td>
        <td>Closure</td>
    </tr>
</table>


## License
"Phone Mania API" is an open-sourced software licensed under the MIT license(https://opensource.org/licenses/MIT).

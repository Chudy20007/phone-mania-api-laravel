<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use JWTAuth;

class ExtendedPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $userToken = $request->bearerToken();

        if (JWTAuth::toUser($userToken) == null) {

            return response()->json($this->makeMessage('Musisz być zalogowany!'), 422);

        } else {
            if (JWTAuth::toUser($userToken)->hasRole('administrator') || JWTAuth::toUser($userToken)->hasRole('pracownik')) {
                return $next($request);
            } else {
                return response()->json($this->makeMessage('Brak dostępu!'), 422);

            }
        }
    }

    private function makeMessage($message)
    {
        return $response = [
            'message' => $message,
        ];
    }
}

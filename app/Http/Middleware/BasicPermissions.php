<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use JWTAuth;

class BasicPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $userToken = $request->bearerToken();

        if (JWTAuth::toUser($userToken) == null) {

            return response()->json($this->makeMessage('Musisz być zalogowany!'), 422);

        } else {
                return $next($request);
        }
    }

    private function makeMessage($message)
    {
        return $response = [
            'message' => $message,
        ];
    }
}

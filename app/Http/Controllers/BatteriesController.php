<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\BatteryCreateRequest;
use App\Http\Requests\BatteryUpdateRequest;
use App\Repositories\BatteryRepository;
use App\Validators\BatteryValidator;

/**
 * Class BatteriesController.
 *
 * @package namespace App\Http\Controllers;
 */
class BatteriesController extends Controller
{
    /**
     * @var BatteryRepository
     */
    protected $repository;

    /**
     * @var BatteryValidator
     */
    protected $validator;

    /**
     * BatteriesController constructor.
     *
     * @param BatteryRepository $repository
     * @param BatteryValidator $validator
     */
    public function __construct(BatteryRepository $repository, BatteryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $batteries = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $batteries,
            ]);
        }

        return view('batteries.index', compact('batteries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BatteryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(BatteryCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $battery = $this->repository->create($request->all());

            $response = [
                'message' => 'Rodzaj baterii został utworzony pomyślnie!',
                'data'    => $battery->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $battery = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $battery,
            ]);
        }

        return view('batteries.show', compact('battery'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $battery = $this->repository->find($id);

        return view('batteries.edit', compact('battery'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BatteryUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(BatteryUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $battery = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Rodzaj baterii został zaktualizowany pomyślnie!',
                'data'    => $battery->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Rodzaj baterii został usunięty pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Rodzaj baterii został usunięty pomyślnie!');
    }
}

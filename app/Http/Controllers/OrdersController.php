<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\OrderCreateRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Repositories\OrderRepository;
use JWTAuth;
use App\Repositories\UserRepository;
use App\Repositories\PaymentRepository;
use App\Validators\OrderValidator;
use DB;

/**
 * Class OrdersController.
 *
 * @package namespace App\Http\Controllers;
 */
class OrdersController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $repository;
    protected $userRepository;
    protected $paymentRepository;
    /**
     * @var OrderValidator
     */
    protected $validator;

    /**
     * OrdersController constructor.
     *
     * @param OrderRepository $repository
     * @param OrderValidator $validator
     */
    public function __construct(OrderRepository $repository, UserRepository $user, PaymentRepository $paymentRepository,
     OrderValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->userRepository = $user;
        $this->paymentRepository = $paymentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $orders = $this->repository->with(['payment'])->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $orders,
            ]);
        }

        return view('orders.index', compact('orders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  OrderCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(OrderCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $order = $this->repository->create($request->all());
            $products = $request->products;
            $userID =1;
            $order->user()->attach($userID);
          
            for($i=0; $i < count($products); $i++)
            {
             $order->products()->attach([
                $products[$i]['id'] =>[
                    'count' => $products[$i]['count'],
                    'discount' => 0,
                    'order_id' => $order->id
                ]
                ]);   
            }

            $response = [
                'message' => 'Zamówienie zostało przyjęte!',
                'data'    => $order->toArray(),
            ];
            $user = $this->userRepository->find(1);
            $user->sendOrderStatusAcceptedNotification($order,'przyjęto');

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = $this->repository->with(['payment','products.manufacturer'])->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $order,
            ]);
        }

        return view('orders.show', compact('order'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = $this->repository->find($id)->with(['payment','products']);

        return view('orders.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  OrderUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(OrderUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $order = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Zamówienie zaktualizowano pomyślnie!',
                'data'    => $order->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Zamówienie zostało usunięte pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Zamówienie zostało usunięte pomyślnie!');
    }

    public function removeProductFromOrder($orderID, $productID){
        $order = $this->repository->with(['products'])->find($orderID); 
        $order->products()->detach($productID);
        
        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Produkt został usunięte pomyślnie ze zlecenia!',
            ]);
        }
        return redirect()->back()->with('message', 'Produkt został usunięte pomyślnie ze zlecenia!');
    }

    public function userOrders(Request $request)
    {
        $loggedUser = JWTAuth::toUser($request->bearerToken());
        $orders = $loggedUser->orders()->with(['payment' => function($q){
            $q->select(array('id','name'));
        }])->get();
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $orders
            ]);
        }
        return redirect()->back()->with('message', 'Produkt został usunięte pomyślnie ze zlecenia!');
    }

    public function updateOrderStatus(Request $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $order = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Status zamówienia zaktualizowano pomyślnie!',
                'data'    => $order->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

}

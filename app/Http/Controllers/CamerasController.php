<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\CameraCreateRequest;
use App\Http\Requests\CameraUpdateRequest;
use App\Repositories\CameraRepository;
use App\Validators\CameraValidator;

/**
 * Class CamerasController.
 *
 * @package namespace App\Http\Controllers;
 */
class CamerasController extends Controller
{
    /**
     * @var CameraRepository
     */
    protected $repository;

    /**
     * @var CameraValidator
     */
    protected $validator;

    /**
     * CamerasController constructor.
     *
     * @param CameraRepository $repository
     * @param CameraValidator $validator
     */
    public function __construct(CameraRepository $repository, CameraValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $cameras = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $cameras,
            ]);
        }

        return view('cameras.index', compact('cameras'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CameraCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CameraCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $camera = $this->repository->create($request->all());

            $response = [
                'message' => 'Typ aparatu został utworzony pomyślnie!',
                'data'    => $camera->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $camera = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $camera,
            ]);
        }

        return view('cameras.show', compact('camera'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $camera = $this->repository->find($id);

        return view('cameras.edit', compact('camera'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CameraUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CameraUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $camera = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Typ aparatu został zaktualizowany pomyślnie!',
                'data'    => $camera->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Typ aparatu został usunięty pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Typ aparatu został usunięty pomyślnie!');
    }
}

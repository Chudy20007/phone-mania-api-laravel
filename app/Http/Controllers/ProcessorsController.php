<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ProcessorCreateRequest;
use App\Http\Requests\ProcessorUpdateRequest;
use App\Repositories\ProcessorRepository;
use App\Validators\ProcessorValidator;

/**
 * Class ProcessorsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProcessorsController extends Controller
{
    /**
     * @var ProcessorRepository
     */
    protected $repository;

    /**
     * @var ProcessorValidator
     */
    protected $validator;

    /**
     * ProcessorsController constructor.
     *
     * @param ProcessorRepository $repository
     * @param ProcessorValidator $validator
     */
    public function __construct(ProcessorRepository $repository, ProcessorValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $processors = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $processors,
            ]);
        }

        return view('processors.index', compact('processors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProcessorCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ProcessorCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $processor = $this->repository->create($request->all());

            $response = [
                'message' => 'Procesor został dodany pomyślnie!',
                'data'    => $processor->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $processor = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $processor,
            ]);
        }

        return view('processors.show', compact('processor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $processor = $this->repository->find($id);

        return view('processors.edit', compact('processor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProcessorUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ProcessorUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $processor = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Procesor został zaktualizowany pomyślnie!',
                'data'    => $processor->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Procesor został usnięty pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Procesor został usnięty pomyślnie!');
    }
}

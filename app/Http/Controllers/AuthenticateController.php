<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
class AuthenticateController extends Controller
{
    public function __construct()
        {

        }

        /**
 * Returns a refreshed token.
 *
 * @param	Request 	$request
 * @return 	Response $response
 */
    public function refreshToken(Request $request) {
        $current_token  = JWTAuth::getToken();
        $token          = JWTAuth::refresh($current_token);
        return response()->json([
        'message' => 'Token odświeżony!',
        'access_token' =>  $token], 200);
    }
}

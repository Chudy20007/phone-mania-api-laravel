<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ManufacturerCreateRequest;
use App\Http\Requests\ManufacturerUpdateRequest;
use App\Repositories\ManufacturerRepository;
use App\Validators\ManufacturerValidator;

/**
 * Class ManufacturersController.
 *
 * @package namespace App\Http\Controllers;
 */
class ManufacturersController extends Controller
{
    /**
     * @var ManufacturerRepository
     */
    protected $repository;

    /**
     * @var ManufacturerValidator
     */
    protected $validator;

    /**
     * ManufacturersController constructor.
     *
     * @param ManufacturerRepository $repository
     * @param ManufacturerValidator $validator
     */
    public function __construct(ManufacturerRepository $repository, ManufacturerValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $manufacturers = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $manufacturers,
            ]);
        }

        return view('manufacturers.index', compact('manufacturers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ManufacturerCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ManufacturerCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $manufacturer = $this->repository->create($request->all());

            $response = [
                'message' => 'Producent został utworzony pomyślnie!',
                'data'    => $manufacturer->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $manufacturer = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $manufacturer,
            ]);
        }

        return view('manufacturers.show', compact('manufacturer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $manufacturer = $this->repository->find($id);

        return view('manufacturers.edit', compact('manufacturer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ManufacturerUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ManufacturerUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $manufacturer = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Producent został zaktualizowany pomyślnie!',
                'data'    => $manufacturer->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Producent został usunięty pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Producent został usunięty pomyślnie!');
    }
}

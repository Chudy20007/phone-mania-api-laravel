<?php

namespace App\Http\Controllers;

use App\Entities\User;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Mail\VerifyMail;
use App\Repositories\UserRepository;
use App\Repositories\VerifyUserRepository;
use App\Validators\UserValidator;
use Illuminate\Http\Request;
use JWTAuth;
use Mail;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class UsersController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $repository;
    protected $verifyUserRepository;
    /**
     * @var UserValidator
     */
    protected $validator;

    /**
     * UsersController constructor.
     *
     * @param UserRepository $repository
     * @param UserValidator $validator
     */
    public function __construct(UserRepository $repository, VerifyUserRepository $verifyUser, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->verifyUserRepository = $verifyUser;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $loggedUser = JWTAuth::toUser($request->bearerToken());
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $users = $this->repository->all()->makeVisible(
            [
                'id', 'email', 'first_name', 'last_name', 'street', 'street_number', 'post_code', 'city','verified'
            ]
        );

        return response()->json([
            'data' => $users,
        ]);

        //return view('users.index', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  UserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(UserCreateRequest $request)
    {

        $loggedUser = JWTAuth::toUser($request->bearerToken());
        if ($loggedUser->can('store', User::class)) {
            try {

                $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
                $request->merge(['password' => bcrypt($request->password)]);

                $user = $this->repository->create($request->all());

                $verifyUser = $this->verifyUserRepository->create([
                    'user_id' => $user->id,
                    'token' => str_random(40),
                ]);
                //return response()->json($user->with('verifyUser'));
                $registeredUser = $this->repository->with('verifyUser')->find($user->id);
                Mail::to($registeredUser->email)->send(new VerifyMail($registeredUser));
                $response = [
                    'message' => 'Użytkownik został utworzony pomyślnie! W celu aktywacji konta została wysłana wiadomość na podany adres e-mail!',
                    'data' => $user->toArray(),
                ];

                if ($request->wantsJson()) {

                    return response()->json($response);
                }

                return redirect()->back()->with('message', $response['message']);
            } catch (ValidatorException $e) {
                if ($request->wantsJson()) {
                    return response()->json([
                        'error' => true,
                        'message' => $e->getMessageBag(),
                    ]);
                }

                return redirect()->back()->withErrors($e->getMessageBag())->withInput();
            }
        } else {
            $response = [
                'message' => 'Brak dostępu!',
            ];

            if ($request->wantsJson()) {

                return response()->json($response, 401);
            }
        }
    }

    public function verifyUser($token)
    {
        $verifyUser = $this->verifyUserRepository->with('user')->findWhere(['token' => $token])->first();

        if (isset($verifyUser)) {
            $user = $verifyUser->user;
            if (!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                return response()->json([
                    'error' => true,
                    'message' => "Konto zostało aktywowane!",
                ], 200);
            } else {
                return response()->json([
                    'error' => true,
                    'message' => "Token wygasł lub konto zostało już aktywowane!",
                ], 401);
            }
        } else {
            return response()->json([
                'error' => true,
                'message' => "Żądanie nie zostało zrealizowane!",
            ], 401);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $loggedUser = JWTAuth::toUser($request->bearerToken());
        $user = $this->repository->find($id);
        if ($loggedUser->can('view', $user)) {

            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $user,
                ]);
            }
        } else {
            $response = [
                'message' => 'Brak dostępu!',
            ];

            if ($request->wantsJson()) {

                return response()->json($response, 401);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->repository->find($id);

        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UserUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $loggedUser = JWTAuth::toUser($request->bearerToken());
        $user = $this->repository->find($id);
        if ($loggedUser->can('update', $user)) {

            try {

                $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
                $request->merge(['password' => bcrypt($request->password)]);
                $user = $this->repository->update($request->all(), $id);

                $response = [
                    'message' => 'Użytkownik został zaktualizowany pomyślnie!',
                    'data' => $user->toArray(),
                ];

                if ($request->wantsJson()) {

                    return response()->json($response);
                }

                return redirect()->back()->with('message', $response['message']);
            } catch (ValidatorException $e) {

                if ($request->wantsJson()) {

                    return response()->json([
                        'error' => true,
                        'message' => $e->getMessageBag(),
                    ]);
                }

                return redirect()->back()->withErrors($e->getMessageBag())->withInput();
            }
        } else {
            $response = [
                'message' => 'Brak dostępu!',
            ];

            if ($request->wantsJson()) {

                return response()->json($response, 401);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $loggedUser = JWTAuth::toUser($request->bearerToken());
        $deleted = $this->repository->find($id);
        if ($loggedUser->can('delete', $deleted)) {
            $deleted = $this->repository->delete($id);
            if (request()->wantsJson()) {

                return response()->json([
                    'message' => 'Użytkownik został usunięty pomyślnie!',
                    'deleted' => $deleted,
                ]);
            }

            return redirect()->back()->with('message', 'Użytkownik został usunięty pomyślnie!');
        } else {
            $response = [
                'message' => 'Brak dostępu!',
            ];

            if ($request->wantsJson()) {

                return response()->json($response, 401);
            }
        }
    }
}

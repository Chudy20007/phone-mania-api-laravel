<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\RamCreateRequest;
use App\Http\Requests\RamUpdateRequest;
use App\Repositories\RamRepository;
use App\Validators\RamValidator;

/**
 * Class RamsController.
 *
 * @package namespace App\Http\Controllers;
 */
class RamsController extends Controller
{
    /**
     * @var RamRepository
     */
    protected $repository;

    /**
     * @var RamValidator
     */
    protected $validator;

    /**
     * RamsController constructor.
     *
     * @param RamRepository $repository
     * @param RamValidator $validator
     */
    public function __construct(RamRepository $repository, RamValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $rams = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $rams,
            ]);
        }

        return view('rams.index', compact('rams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RamCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(RamCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $ram = $this->repository->create($request->all());

            $response = [
                'message' => 'Pamięć RAM została utworzona pomyślnie!',
                'data'    => $ram->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ram = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $ram,
            ]);
        }

        return view('rams.show', compact('ram'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ram = $this->repository->find($id);

        return view('rams.edit', compact('ram'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RamUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(RamUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $ram = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Pamięć RAM zaktualizowana pomyślnie',
                'data'    => $ram->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Pamięć RAM została usunięta pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Ram deleted.');
    }
}

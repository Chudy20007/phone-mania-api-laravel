<?php

namespace App\Http\Controllers;

use App\Http\Requests\SmartphoneCreateRequest;
use App\Http\Requests\SmartphoneUpdateRequest;
use App\Repositories\AttachmentRepository;
use App\Repositories\BatteryRepository;
use App\Repositories\CameraRepository;
use App\Repositories\DisplayRepository;
use App\Repositories\ManufacturerRepository;
use App\Repositories\MemoryRepository;
use App\Repositories\ProcessorRepository;
use App\Repositories\RamRepository;
use App\Repositories\ResolutionRepository;
use App\Repositories\SmartphoneRepository;
use App\Repositories\SystemRepository;
use App\Validators\SmartphoneValidator;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Storage;

/**
 * Class SmartphonesController.
 *
 * @package namespace App\Http\Controllers;
 */
class SmartphonesController extends Controller
{
    /**
     * @var SmartphoneRepository
     */
    protected $repository;
    protected $processorRepository;
    /**
     * @var SmartphoneValidator
     */
    protected $validator;

    /**
     * SmartphonesController constructor.
     *
     * @param SmartphoneRepository $repository
     * @param SmartphoneValidator $validator
     */
    public function __construct(SmartphoneRepository $repository,
        SmartphoneValidator $validator) {

        $this->repository = $repository;

        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $smartphones = $this->repository->with(['battery', 'attachments', 'camera', 'display', 'manufacturer', 'memory', 'processor', 'ram', 'resolution', 'system'])->get();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $smartphones,
            ]);
        }

        return view('smartphones.index', compact('smartphones'));
    }

    public function create(ProcessorRepository $processorRepository, ManufacturerRepository $manufacturerRepository,
        CameraRepository $cameraRepository, AttachmentRepository $attachmentRepository
        , BatteryRepository $batteryRepository, SystemRepository $systemRepository, ResolutionRepository $resolutionRepository,
        MemoryRepository $memoryRepository, RamRepository $ramRepository, DisplayRepository $displayRepository) {
        $this->processorRepository = $processorRepository;
        $this->manufacturerRepository = $manufacturerRepository;
        $this->cameraRepository = $cameraRepository;
        $this->batteryRepository = $batteryRepository;
        $this->resolutionRepository = $resolutionRepository;
        $this->ramRepository = $ramRepository;
        $this->systemRepository = $systemRepository;
        $this->displayRepository = $displayRepository;
        $this->memoryRepository = $memoryRepository;
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $processors = $this->processorRepository->all();
        $manufacturers = $this->manufacturerRepository->all();
        $cameras = $this->cameraRepository->all();
        $batteries = $this->batteryRepository->all();
        $resolutions = $this->resolutionRepository->all();
        $rams = $this->ramRepository->all();
        $systems = $this->systemRepository->all();
        $displays = $this->displayRepository->all();
        $memories = $this->memoryRepository->all();
        if (request()->wantsJson()) {

            return response()->json([
                'data' => [
                    'processors' => $processors,
                    'manufacturers' => $manufacturers,
                    'cameras' => $cameras,
                    'batteries' => $batteries,
                    'resolutions' => $resolutions,
                    'rams' => $rams,
                    'systems' => $systems,
                    'displays' => $displays,
                    'memories' => $memories,
                ],
            ]);
        }

        return view('smartphones.create', compact('smartphones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SmartphoneCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(SmartphoneCreateRequest $request, AttachmentRepository $attachmentRepository)
    {
 
        $mainFile = Input::file('main_photo');
        $photos = Input::file('smartphone_photos');
        $this->attachmentRepository = $attachmentRepository;

        try {
            if ($this->repository->orderBy('id', 'desc')->first() != null) {
                $filePath = 'main_photos\\' . $this->repository->orderBy('id', 'desc')->first()->id;
            } else {
                $filePath = 'main_photos\\1';
            }

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request->merge(['photo' => $filePath . '.' . $mainFile->getClientOriginalExtension()]);

            $smartphone = $this->repository->create($request->except(['smartphone_photos', 'main_photo', '0']));
            $mainFile->move(env('FILES_PATH') . 'main_photos', $smartphone->id . '.' . $mainFile->getClientOriginalExtension());
            $this->storeNewAttachments($request, $smartphone, $request->bearerToken());
            $response = [
                'message' => 'Telefon dodany pomyślnie!',
                'data' => $smartphone->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $smartphone = $this->repository->with(['battery', 'camera', 'comments.user','attachments', 'display', 'manufacturer', 'memory', 'processor', 'ram', 'resolution', 'system'])
        ->find($id);
        $smartphone['rate'] = $smartphone->averageRating();

        $smartphone['rates_count'] = $smartphone->ratesCount();
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $smartphone,
            ]);
        }

        return view('smartphones.show', compact('smartphone'));
    }
    public function getPhotos($img)
    {
        return response()->file('C:\xampp\htdocs\phone-mania-api-laravel\storage\img\photos\\' . $img);
    }

    public function getMainPhoto($img)
    {
        return response()->file('C:\xampp\htdocs\phone-mania-api-laravel\storage\img\main_photos\\' . $img);
    }
    private function storeNewAttachments($request, $createdSmartphone, $token)
    {

        $attachmentID = $this->attachmentRepository->orderBy('id', 'desc')->first()['id'];
        if ($attachmentID == null) {
            $attachmentID = 0;
        }

        $counter;

        foreach ($request->smartphone_photos as $file) {

            $attachmentID = $attachmentID + 1;
            $source = '\photos\\' . $attachmentID . '.' . $file->getClientOriginalExtension();

            $attachmentsToCreate =
                [
                'source' => $source,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];
            $file->move(env('FILES_PATH') . 'photos', $attachmentID . '.' . $file->getClientOriginalExtension());
            $this->attachmentRepository->create($attachmentsToCreate);
        }

        $attach2 = $this->attachmentRepository->orderBy('id', 'desc')->paginate(count($request->smartphone_photos));

        for ($i = 0; $i < $attach2->count(); $i++) {
            $createdSmartphone->attachments()->attach($attach2[$i]->id);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id, ProcessorRepository $processorRepository, ManufacturerRepository $manufacturerRepository, CameraRepository $cameraRepository
        , BatteryRepository $batteryRepository, SystemRepository $systemRepository, ResolutionRepository $resolutionRepository, MemoryRepository $memoryRepository, RamRepository $ramRepository, DisplayRepository $displayRepository) {
        $this->processorRepository = $processorRepository;
        $this->manufacturerRepository = $manufacturerRepository;
        $this->cameraRepository = $cameraRepository;
        $this->batteryRepository = $batteryRepository;
        $this->resolutionRepository = $resolutionRepository;
        $this->ramRepository = $ramRepository;
        $this->systemRepository = $systemRepository;
        $this->displayRepository = $displayRepository;
        $this->memoryRepository = $memoryRepository;

        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $smartphone = $this->repository->find($id);
        $processors = $this->processorRepository->all();
        $manufacturers = $this->manufacturerRepository->all();
        $cameras = $this->cameraRepository->all();
        $batteries = $this->batteryRepository->all();
        $resolutions = $this->resolutionRepository->all();
        $rams = $this->ramRepository->all();
        $systems = $this->systemRepository->all();
        $displays = $this->displayRepository->all();
        $memories = $this->memoryRepository->all();
        if (request()->wantsJson()) {

            return response()->json([
                'data' => [
                    'smartphone' => $smartphone,
                    'processors' => $processors,
                    'manufacturers' => $manufacturers,
                    'cameras' => $cameras,
                    'batteries' => $batteries,
                    'resolutions' => $resolutions,
                    'rams' => $rams,
                    'systems' => $systems,
                    'displays' => $displays,
                    'memories' => $memories,
                ],
            ]);
        }

        return view('smartphones.edit', compact('smartphone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SmartphoneUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(SmartphoneUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $smartphone = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Telefon zaktualizowany pomyślnie!',
                'data' => $smartphone->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Telefon został usunięty pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Smartphone deleted.');
    }

    public function findProductsByManufacturers(Request $request)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $manufacturers = $request->all();
        if ($manufacturers == null) {
            $smartphones = $this->repository
                ->with(['battery', 'attachments', 'camera', 'display', 'manufacturer', 'memory', 'processor', 'ram', 'resolution', 'system'])->get();
        } else {
            $smartphones = $this->repository
                ->with(['battery', 'attachments', 'camera', 'display', 'manufacturer', 'memory', 'processor', 'ram', 'resolution', 'system'])
                ->whereHas('manufacturer', function ($query) use ($manufacturers) {
                    $query->whereIn('name', $manufacturers);
                })
                ->get();
        }

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $smartphones,
            ]);
        }

    }

    public function findProducts($value)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        if ($value == null) {
            $smartphones = $this->repository
                ->with(['battery', 'attachments', 'camera', 'display', 'manufacturer', 'memory', 'processor', 'ram', 'resolution', 'system'])->get();
        } else {
            $smartphones = $this->repository
                ->with(['battery', 'attachments', 'camera', 'display', 'manufacturer', 'memory', 'processor', 'ram', 'resolution', 'system'])
                ->findWhere([['name', 'LIKE', '%' . $value . '%']]);
        }

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $smartphones,
            ]);
        }

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\DisplayCreateRequest;
use App\Http\Requests\DisplayUpdateRequest;
use App\Repositories\DisplayRepository;
use App\Validators\DisplayValidator;

/**
 * Class DisplaysController.
 *
 * @package namespace App\Http\Controllers;
 */
class DisplaysController extends Controller
{
    /**
     * @var DisplayRepository
     */
    protected $repository;

    /**
     * @var DisplayValidator
     */
    protected $validator;

    /**
     * DisplaysController constructor.
     *
     * @param DisplayRepository $repository
     * @param DisplayValidator $validator
     */
    public function __construct(DisplayRepository $repository, DisplayValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $displays = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $displays,
            ]);
        }

        return view('displays.index', compact('displays'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DisplayCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(DisplayCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $display = $this->repository->create($request->all());

            $response = [
                'message' => 'Typ wyświetlacza został utworzony pomyślnie!',
                'data'    => $display->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $display = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $display,
            ]);
        }

        return view('displays.show', compact('display'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $display = $this->repository->find($id);

        return view('displays.edit', compact('display'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DisplayUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(DisplayUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $display = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Typ wyświetlacza został edytowany pomyślnie!',
                'data'    => $display->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Typ wyświetlacza został usunięty pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Typ wyświetlacza został usunięty pomyślnie!');
    }
}

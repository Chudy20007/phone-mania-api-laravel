<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\SystemCreateRequest;
use App\Http\Requests\SystemUpdateRequest;
use App\Repositories\SystemRepository;
use App\Validators\SystemValidator;

/**
 * Class SystemsController.
 *
 * @package namespace App\Http\Controllers;
 */
class SystemsController extends Controller
{
    /**
     * @var SystemRepository
     */
    protected $repository;

    /**
     * @var SystemValidator
     */
    protected $validator;

    /**
     * SystemsController constructor.
     *
     * @param SystemRepository $repository
     * @param SystemValidator $validator
     */
    public function __construct(SystemRepository $repository, SystemValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $systems = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $systems,
            ]);
        }

        return view('systems.index', compact('systems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SystemCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(SystemCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $system = $this->repository->create($request->all());

            $response = [
                'message' => 'System został dodany pomyślnie!',
                'data'    => $system->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $system = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $system,
            ]);
        }

        return view('systems.show', compact('system'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $system = $this->repository->find($id);

        return view('systems.edit', compact('system'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SystemUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(SystemUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $system = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'System został edytowany pomyślnie!',
                'data'    => $system->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'System został usunięty pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'System został usunięty pomyślnie!');
    }
}

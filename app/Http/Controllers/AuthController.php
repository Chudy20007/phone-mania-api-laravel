<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
class AuthController extends Controller
{
	/**
	 * Get a JWT token via given credentials.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */

	public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','logout']]);
    }
	public function login(Request $request)
	{
		$credentials = $request->only('email', 'password');
		
		if ($token = JWTAuth::attempt($credentials)) {
			return $this->respondWithToken($token);
		}
		
		return response()->json(['message' => 'Błędny login lub hasło!'], 401);
	}
	/**
	 * Get the authenticated User
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function me()
	{
		return response()->json($this->guard()->user());
	}

	public function getUserRole()
	{
		return response()->json($this->guard()->user()->roles()->first()->name);
	}
	/**
	 * Log the user out (Invalidate the token)
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function logout()
	{
		JWTAuth::invalidate(JWTAuth::getToken());
		return response()->json(['message' => 'Użytkownik wylogowany pomyślnie!']);
	}
	/**
	 * Refresh a token.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function refresh()
	{
		return $this->respondWithToken($this->guard()->refresh());
	}
	/**
	 * Get the token array structure.
	 *
	 * @param  string $token
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function respondWithToken($token)
	{
		//$loggedUser = JWTAuth::toUser($token);
	
		return response()->json([
			'access_token' => $token,
			'role' => $this->getUserRole(),
			'token_type' => 'bearer',
			'message' => 'Użytkownik zalogowany pomyślnie!',
			'expires_in' => auth('api')->factory()->getTTL() * 10
		]);
	}
	/**
	 * Get the guard to be used during authentication.
	 *
	 * @return \Illuminate\Contracts\Auth\Guard
	 */
	public function guard()
	{
		return Auth::guard();
	}
}
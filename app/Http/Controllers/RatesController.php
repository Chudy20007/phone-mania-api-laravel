<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\RateCreateRequest;
use App\Http\Requests\RateUpdateRequest;
use App\Repositories\RateRepository;
use App\Validators\RateValidator;
use App\Repositories\CommentRepository;
use App\Validators\CommentValidator;
use JWTAuth;

/**
 * Class RatesController.
 *
 * @package namespace App\Http\Controllers;
 */
class RatesController extends Controller
{
    /**
     * @var RateRepository
     */
    protected $repository;

    /**
     * @var RateValidator
     */
    protected $validator;

        /**
     * @var CommentRepository
     */
    protected $commentRepository;

    /**
     * @var CommentValidator
     */
    protected $commentValidator;

    /**
     * RatesController constructor.
     *
     * @param RateRepository $repository
     * @param RateValidator $validator
     * @param CommentRepository $commentRepository
     * @param CommentValidator $commentValidator
     */
    public function __construct(RateRepository $repository, RateValidator $validator,
    CommentRepository $commentRepository, CommentValidator $commentValidator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->commentRepository = $commentRepository;
        $this->commentValidator = $commentValidator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $rates = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $rates,
            ]);
        }

        return view('rates.index', compact('rates'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RateCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(RateCreateRequest $request)
    {
        try {
            $loggedUser = JWTAuth::toUser($request->bearerToken());
            $request['user_id'] = $loggedUser->id;
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
           
            $this->commentValidator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $rate = $this->repository->create($request->all());
            $comment = $this->commentRepository->create($request->all());

            $response = [
                'message' => 'Ocena wraz z komentarzem wystawiono pomyślnie!',
                'data'    => $rate->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rate = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $rate,
            ]);
        }

        return view('rates.show', compact('rate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rate = $this->repository->find($id);

        return view('rates.edit', compact('rate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RateUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(RateUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $rate = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Ocena zaktualizowana pomyślnie!',
                'data'    => $rate->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Ocena została usunięta!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Ocena została usunięta!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ResolutionCreateRequest;
use App\Http\Requests\ResolutionUpdateRequest;
use App\Repositories\ResolutionRepository;
use App\Validators\ResolutionValidator;

/**
 * Class ResolutionsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ResolutionsController extends Controller
{
    /**
     * @var ResolutionRepository
     */
    protected $repository;

    /**
     * @var ResolutionValidator
     */
    protected $validator;

    /**
     * ResolutionsController constructor.
     *
     * @param ResolutionRepository $repository
     * @param ResolutionValidator $validator
     */
    public function __construct(ResolutionRepository $repository, ResolutionValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $resolutions = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $resolutions,
            ]);
        }

        return view('resolutions.index', compact('resolutions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ResolutionCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ResolutionCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $resolution = $this->repository->create($request->all());

            $response = [
                'message' => 'Rozdzielczość została utworzona pomyślnie!',
                'data'    => $resolution->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $resolution = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $resolution,
            ]);
        }

        return view('resolutions.show', compact('resolution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $resolution = $this->repository->find($id);

        return view('resolutions.edit', compact('resolution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ResolutionUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ResolutionUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $resolution = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Rozdzielczość została zaktualizowana pomyślnie!',
                'data'    => $resolution->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Rozdzielczość została usunięta pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Rozdzielczość została usunięta pomyślnie!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\MemoryCreateRequest;
use App\Http\Requests\MemoryUpdateRequest;
use App\Repositories\MemoryRepository;
use App\Validators\MemoryValidator;

/**
 * Class MemoriesController.
 *
 * @package namespace App\Http\Controllers;
 */
class MemoriesController extends Controller
{
    /**
     * @var MemoryRepository
     */
    protected $repository;

    /**
     * @var MemoryValidator
     */
    protected $validator;

    /**
     * MemoriesController constructor.
     *
     * @param MemoryRepository $repository
     * @param MemoryValidator $validator
     */
    public function __construct(MemoryRepository $repository, MemoryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $memories = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $memories,
            ]);
        }

        return view('memories.index', compact('memories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  MemoryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(MemoryCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $memory = $this->repository->create($request->all());

            $response = [
                'message' => 'Rozmiar pamięci został utworzony pomyślnie!',
                'data'    => $memory->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $memory = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $memory,
            ]);
        }

        return view('memories.show', compact('memory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $memory = $this->repository->find($id);

        return view('memories.edit', compact('memory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  MemoryUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(MemoryUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $memory = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Rozmiar pamięci został zaktualizowany pomyślnie!',
                'data'    => $memory->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Rozmiar pamięci został usunięty pomyślnie!',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Rozmiar pamięci został usunięty pomyślnie!');
    }
}

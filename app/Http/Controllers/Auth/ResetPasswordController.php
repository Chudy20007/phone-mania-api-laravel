<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
            return response()->json(['token' => $token, 'email' => $request->email],200);
    }

    protected function sendResetResponse($response)
    {   
        return response()->json([
                'message' => "Hasło zostało zresetowane!"
         ],200);
     }
     protected function sendResetFailedResponse(Request $request, $response)
     {
        return response()->json([
                'message' => "Błędny token lub adres e-mail!"
        ],401);
     }

    
}

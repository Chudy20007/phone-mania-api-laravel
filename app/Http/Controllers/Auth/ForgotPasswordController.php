<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\Repositories\UserRepository;
use Session;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */
    protected $userRepository;
    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('user.passwords.email');
    }  
    
    public function sendResetLinkEmail(UserRepository $user, ForgotPasswordRequest $request)
{
    $this->userRepository = $user;

            $userCheck = $this->userRepository->findWhere(['email' => $request->email])->first();
    
    if (!$userCheck->verified) {
        return response()->json([
            'message' => 'Twoje konto nie zostało aktywowane!',
        ],401);
    } else {
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if ($response === Password::RESET_LINK_SENT) {
                return response()->json([
                    'message' => 'Wiadomość została wysłana na podany adres e-mail!',
                ],200);
            } 

            return response()->json([
                'message' => 'Wystąpił błąd!',
            ],401);
        }   
} 

    public function broker()
    {
         return Password::broker('users');
    }
    private function makeMessage($message)
    {
        return $response = [
            'message' => $message,
        ];
    }

    
}

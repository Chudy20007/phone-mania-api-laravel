<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                {
                    return [];
                }
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                      
                    ];
                }
            case 'PUT':
                {
                    return [];
                }
            case 'PATCH':
                {
                    return [
                       
                    ];
                }

            default:break;
        }
       
    }

    public function messages()
    {

        return [
            'email.required' => 'Podaj adres e-mail!',
            'email.regex' => 'Podaj poprawny adres e-mail!',
            'email.exists' => 'Brak użytkownika o podanym adresie e-mail!'
        ];

    }
}

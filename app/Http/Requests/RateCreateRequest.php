<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use JWTAuth;

class RateCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = JWTAuth::toUser($this->bearerToken());

        $this['user_id'] = $user->id;
        $user_id = $user->id;
        $product_id = $this->product_id;
        return [
            'rate' => ['required', 'regex:/^[+-]?([0-9]*[.])?[0-9]+$/'],
            'comment' => ['required'],
            'product_id' => ['required', Rule::unique('rates')
            ->where(function ($query) use ($user_id, $product_id)  {
             return $query
            ->where('product_id','=', $product_id)
            ->where('user_id', '=', $user_id);
            })],
        ];
    }

    public function messages()
    {
        return [
            'rate.required' => 'Podaj ocenę dla produktu!',
            'product_id.unique' => 'Oceniłeś już dany produkt!',
            'rate.regex' => 'Podaj poprawną ocenę dla produktu!',
            'comment.required' => 'Komentarz jest wymagany!',
            'product_id.required' => 'Podaj, który produkt chcesz ocenić!',
        ];
    }

}

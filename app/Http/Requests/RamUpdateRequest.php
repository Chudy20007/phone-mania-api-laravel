<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RamUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'size' => ['required','regex:/^([0-9]{1,2})$/',Rule::unique('rams','size')->ignore($this->ram)]
         ];
    }

    public function messages()
    {
        return [
            'size.regex' => 'Podaj rozmiar dla pamięci RAM!',
            'size.required' => 'Podaj rozmiar dla pamięci RAM!',
            'size.unique' => 'Taka pamieć RAM już istnieje!',
        ];
    }
}

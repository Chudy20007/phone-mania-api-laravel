<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Response;
use Illuminate\Foundation\Http\FormRequest;

class ManufacturerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','regex:/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/',Rule::unique('manufacturers', 'name')],
        ];
    }

    public function messages()
    {
        return [
            'name.regex' => 'Podaj poprawną nazwę dla typu!',
            'name.unique' => 'Taki typ wyświetlacza już istnieje!',
            'name.required' => 'Podaj odpowiednią nazwę!',
        ];
    }
}

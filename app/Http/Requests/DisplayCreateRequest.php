<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Response;
use Illuminate\Foundation\Http\FormRequest;

class DisplayCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','regex:/^([A-ZĄĘŹŻŁÓŁa-ząęźżół]{1,}.[A-ZĄĘŹŻŁÓŁa-ząęźżół]{1,})$/', Rule::unique('displays','name')],
        ];
    }

    public function messages()
    {
        return [
            'name.regex' => 'Podaj poprawną nazwę dla typu!',
            'name.unique' => 'Taki typ wyświetlacza już istnieje!',
            'name.required' => 'Podaj odpowiednią nazwę!',
            
        ];
    }
}

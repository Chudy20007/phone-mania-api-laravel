<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Response;
use Illuminate\Foundation\Http\FormRequest;

class BatteryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', 'regex:/^([A-Z]{1}[a-z]{3,}[a-z]-[a-z]{3,})$/'],
            'capacity' => ['required', 'regex:/^([0-9]{4,})$/'],
        ];
    }

    public function messages()
    {
        return [
            'type.regex' => 'Podaj poprawną nazwę!',
            'capacity.regex' => 'Podaj poprawną pojemność dla baterii!',
            'type.required' => 'Podaj typ dla baterii!',
            'capacity.required' => 'Podaj pojemność!',
        ];
    }
    
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SmartphoneCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => ['required','regex:/[A-Za-z]{3,4}/'],
            'battery_id' => ['required','regex:/^([0-9]{1,})$/'],
            'camera_id' => ['required','regex:/^([0-9]{1,})$/'],
            'display_id' => ['required','regex:/^([0-9]{1,})$/'],
            'manufacturer_id' => ['required','regex:/^([0-9]{1,})$/'],
            'memory_id' => ['required','regex:/^([0-9]{1,})$/'],
            'processor_id' => ['required','regex:/^([0-9]{1,})$/'],
            'ram_id' => ['required','regex:/^([0-9]{1,})$/'],
            'resolution_id' => ['required','regex:/^([0-9]{1,})$/'],
            'system_id' => ['required','regex:/^([0-9]{1,})$/'],
            'main_photo' => ['max:8096', 'image', 'mimes:jpg,jpeg,png,gif'],
            'smartphone_photos.*' => ['max:20480','image', 'mimes:jpg,jpeg,png'],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Podaj nazwę dla systemu operacyjnego!',
            'name.regex' => 'Podaj poprawną nazwę!',
            'battery_id.required' => 'Podaj baterię dla modelu telefonu!',
            'battery_id.regex' => 'Podaj odpowiednią wartość dla baterii!',
            'camera_id.required' => 'Podaj rodzaj aparatu dla modelu telefonu!',
            'camera_id.regex' => 'Podaj odpowiednią wartość dla kamery!',
            'display_id.required' => 'Podaj użyty rodzaj wyświetlacza dla modelu telefonu!',
            'display_id.regex' => 'Podaj odpowiednią wartość dla wyświetlacza!',
            'manufacturer_id.required' => 'Podaj producenta dla modelu telefonu!',
            'manufacturer_id.regex' => 'Podaj odpowiednią wartość dla producenta!',
            'memory_id.required' => 'Podaj użyty rodzaj pamięci dla modelu telefonu!',
            'memory_id.regex' => 'Podaj odpowiednią wartość dla wybranej pamięci wewnętrznej!',
            'processor_id.required' => 'Podaj rodzaj procesora dla modelu telefonu!',
            'processor_id.regex' => 'Podaj odpowiednią wartość dla procesora!',
            'ram_id.required' => 'Podaj użyty rodzaj pamięci RAM dla modelu telefonu!',
            'ram_id.regex' => 'Podaj odpowiednią wartość dla pamięci RAM!',
            'resolution_id.required' => 'Podaj rozmiar wyświetlacza dla modelu telefonu!',
            'resolution_id.regex' => 'Podaj odpowiednią wartość dla wielkości wyświetlacza!',
            'system_id.required' => 'Podaj system operacyjny dla modelu telefonu!',
            'system_id.regex' => 'Podaj odpowiednią wartość dla systemu operacyjnego!',
            'main_photo.max' => 'Maksymalny rozmiar dla zdjęcia tytułowego to 8MB !',
            'main_photo.mime' => 'Zdjęcie tytułowe musi być typu :values',
            'smartphone_photos.max' => 'Maksymalny rozmiar dla zdjęć produktu to 20MB !',
            'smartphone_photos.mimes' => 'Zdjęcia produktu muszą być typu :values',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total_price' => ['required','regex:/^([1-9][0-9]*|0)(\.[0-9]{2})?$/'],
            'products' => ['required'],
            'payment_id' => ['required','regex:/^[0-9]+$/']           
        ];
    }

    public function messages()
    {
        return [
            'total_price.required' => 'Cena za produkty jest wymagana!',
            'total_price.regex' => 'Podana cena jest niepoprawna!',
            'products.required' => 'Brak produktów w danym zleceniu!',
            'payment_id.regex' => 'Metoda płatności jest niepoprawna!',
            'payment_id.required' => 'Metoda płatności jest wymagana!',
        ];
    }
    
}

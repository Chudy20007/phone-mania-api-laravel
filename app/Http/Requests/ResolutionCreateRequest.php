<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Response;
use Illuminate\Foundation\Http\FormRequest;

class ResolutionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'width' => ['required','regex:/^([0-9]{3,5})$/'],
            'height' => ['required','regex:/^([0-9]{3,5})$/']           
        ];
    }

    public function messages()
    {
        return [
            'width.regex' => 'Podaj poprawną szerokość dla wyświetlacza!',
            'width.required' => 'Podaj szerokość dla wyświetlacza!',
            'height.regex' => 'Podaj poprawną wysokość dla wyświetlacza!',
            'height.required' => 'Podaj wysokość dla wyświetlacza!',
        ];
    }
}

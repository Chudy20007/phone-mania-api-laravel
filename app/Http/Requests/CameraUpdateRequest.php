<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Response;
use Illuminate\Foundation\Http\FormRequest;

class CameraUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'front' => ['required', 'regex:/^([0-9A-ZĄĘŹŻŁÓŁa-ząęźżół]{2,})$/'],
            'back' => ['required', 'regex:/^([0-9A-ZĄĘŹŻŁÓŁa-ząęźżół]{2,})$/'],
        ];
    }

    public function messages()
    {
        return [
            'front.regex' => 'Podaj poprawną wartość dla aparatu przedniego!',
            'front.required' => 'Podaj odpowiednią liczbę dla aparatu przedniego!',
            'back.regex' => 'Podaj poprawną wartość dla aparatu tylniego!',
            'back.required' => 'Podaj odpowiednią liczbę dla aparatu tylniego!',
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Response;
use Illuminate\Foundation\Http\FormRequest;


class TagCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/', Rule::unique('tags', 'name')],
        ];
    }

    public function messages()
    {

        return [
            'name.required' => 'Podaj poprawną nazwę dla tagu!',
            'name.unique' => 'Ten tag już istnieje!',
            'name.regex' => 'Podaj poprawną nazwę dla tagu!',
        ];

    }
}

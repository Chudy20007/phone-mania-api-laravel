<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RateUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rate' => ['required', 'regex:/^[+-]?([0-9]*[.])?[0-9]+$/'],
            'comment' => ['required'],
            'product_id' => ['required']
        ];
    }

    
    public function messages()
    {
        return [
            'rate.required' => 'Podaj ocenę dla produktu!',
            'rate.regex' => 'Podaj poprawną ocenę dla produktu!',
            'comment.required' => 'Komentarz jest wymagany!',
            'product_id.required' => 'Podaj, który produkt chcesz ocenić!',
        ];
    }
}

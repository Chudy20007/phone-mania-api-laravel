<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProcessorUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','regex:/[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ]{3,}\s*[a-zA-ZZąćęłńóśźżĄĆĘŁŃÓŚŹŻ0-9]{1,}/', Rule::unique('processors','name')->ignore($this->processor)],
            'threads_count' => ['required','regex:/[0-9]{1,2}/'],
            'threads_info' => ['required','regex:/[A-Za-z]{3,4}/'],
            'gpu_name' => ['required','regex:/[A-Za-z]{3,4}/']
        ];
    }

    public function messages()
    {
        return [
            'name.regex' => 'Podaj poprawną nazwę!',
            'name.required' => 'Nazwa procesora jest wymagana!',
            'name.unique' => 'Taki model procesora już istnieje!',

            'threads_count.regex' => 'Podaj popawną wartość dla rdzeni proceora!',
            'threads_count.required' => 'Liczba rdzeni jest wymagana!',

            'threads_info.regex' => 'Podaj popawną wartość dla rdzeni proceora!',
            'threads_info.required' => 'Liczba rdzeni jest wymagana!',

            'gpu_name.regex' => 'Podaj nazwę układu graficznego!',
            'gpu_name.required' => 'Podaj poprawną nazwę dla układu graficznego!',

        ];
    }
}

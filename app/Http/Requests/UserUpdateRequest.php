<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rule;
use Response;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/'],
            'last_name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/'],
            'email' => ['required', 'email', Rule::unique('users', 'email')->ignore($this->user)],
            'password' => ['required'],
            'street' => ['required','regex:/[A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{2,}/'],
            'street_number' => ['required', 'regex:/[A-ZĄĘĆŹŻŁŃa-ząęćńźżółu0-9]{2,}/'],
            'post_code' => ['required', 'regex:/^([0-9]{2})(-[0-9]{3})$/'],
            'city' => ['required', 'regex:/([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})/'],
        ];
    }

    public function messages()
    {

        return [
            'first_name.regex' => 'Podaj poprawne imię!',
            'last_name.regex' => 'Podaj poprawne nazwisko!',
            'email.regex' => 'Podaj poprawny adres e-mail!',
            'role.regex' => "Podaj poprawną rolę!",
            'first_name.required' => 'Podaj imię!',
            'last_name.required' => 'Podaj nazwisko!',
            'email.required' => 'Podaj email!',
            'password.required' => 'Podaj hasło!',
            'password.confirmed' => 'Hasła się nie zgadzają!',
            'email.unique' => 'Podany e-mail jest już zajęty!',
            'email.email' =>' Podaj poprawny adres e-mail!',
            'street.required' => 'Podaj nazwę ulicy!',
            'street_number.required' => 'Podaj numer ulicy!',
            'post_code.required' => 'Podaj kod pocztowy!',
            'city.required' => 'Podaj nazwę miejscowości!',
            'street.regex' => 'Podaj poprawną nazwę ulicy!',
            'street_number.regex' => 'Podaj poprawny numer ulicy!',
            'post_code.regex' => 'Podaj poprawny kod pocztowy!',
            'city.regex' => 'Podaj poprawną nazwę miejscowości!',
        ];

    }


}

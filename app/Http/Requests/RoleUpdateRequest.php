<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Response;
use Illuminate\Foundation\Http\FormRequest;

class RoleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'regex:/^([a-ząęćńźżółu]{3,})$/', Rule::unique('roles', 'name')->ignore($this->role)],
        ];
    }

    public function messages()
    {
        return [
            'name.regex' => 'Podaj poprawną nazwę dla roli!',
            'name.required' => 'Podaj rolę!',
            'name.unique' => 'Podana rola już istnieje!'
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Response;
use Illuminate\Foundation\Http\FormRequest;

class MemoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'size' => ['required','regex:/^([0-9]{1,4})$/',Rule::unique('memories','size')]            
        ];
    }

    public function messages()
    {
        return [
            'size.regex' => 'Podaj poprawną nazwę dla pamięci!',
            'size.unique' => 'Taka pojemność już istnieje!',
            'size.required' => 'Podaj odpowiednią pojemność dla pamięci!',
        ];
    }
}

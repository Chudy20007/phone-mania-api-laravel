<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'comment' => ['required'],
            'smartphone_id' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            'comment.required' => 'Napisz poprawny komentarz!',
            'smartphone_id.required' => 'Podaj, którego produktu tyczy się komentarz!',
        ];
    }
}

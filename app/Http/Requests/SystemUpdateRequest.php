<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
class SystemUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/', ],
                'version' => ['required', 'regex:/[A-ZĄĘŹŻŁÓŁa-ząęźżół0-9.]{3,}/',Rule::unique('systems', 'version')->ignore($this->system)]
        ];
    }

    public function messages()
    {

        return [
            'name.required' => 'Podaj nazwę dla systemu operacyjnego!',
            'name.regex' => 'Podaj poprawną nazwę dla systemu operacyjnego!',
            'version.unique' => 'Podany system z taką wersją już istnieje!',
            'version.required' => 'Podaj nazwę dla wersji systemu!',
            'version.regex' => 'Podaj poprawną wersję dla systemu!',
        ];

    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Response;

class PaymentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','regex:/[A-Za-ząęóźżćłĄĘŹŻĆŁÓ ]{4,}/', Rule::unique('payments', 'name')->ignore($this->payment)],
        ];
    }

    public function messages()
    {
        return [
            'name.regex' => 'Podaj poprawną nazwę dla typu płatności!',
            'name.required' => 'Nazwa modelu płatności jest wymagana!',
            'name.unique' => 'Taki typ płatności już istnieje!',
        ];
    }
}

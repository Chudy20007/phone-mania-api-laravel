<?php

namespace App\Policies;

use App\Entities\Display;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DisplayPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the display.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function view(User $loggedUser)
    {

        return true;
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can view displays.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function viewDisplays(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can create displays.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function create(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can update the displays.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function update(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can delete the displays.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function delete(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator');
    }
}

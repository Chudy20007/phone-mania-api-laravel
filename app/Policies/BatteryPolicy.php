<?php

namespace App\Policies;

use App\Entities\Battery;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BatteryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the batteries.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function view(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can view batteries.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function viewBatteries(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can create batteries.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function create(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can update the batteries.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function update(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can delete the batteries.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function delete(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator');
    }
}

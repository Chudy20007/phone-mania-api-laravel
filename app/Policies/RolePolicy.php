<?php

namespace App\Policies;

use App\Entities\Role;
use App\Entities\User;

class RolePolicy
{
    // use HandlesAuthorization;

    /**
     * Determine whether the user can view the role.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Role  $role
     * @return mixed
     */
    public function view(User $user, Role $role)
    {
        return $user->hasRole('administrator') || $user->hasRole('teacher');
    }
    public function viewRoles(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('teacher');
    }
    /**
     * Determine whether the user can create roles.
     *
     * @param  \App\Entities\User  $user
     * @return mixed
     */

    public function create(User $user)
    {
        return $user->hasRole('administrator');
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Role  $role
     * @return mixed
     */
    public function update(User $user, Role $role)
    {
        return $user->hasRole('administrator') && $role->name != 'pracownik' && $role->name != 'administrator' && $role->name != 'użytkownik';
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \App\Entities\User  $user
     * @param  \App\Entities\Role  $role
     * @return mixed
     */
    public function delete(User $user, Role $role)
    {

        return $user->hasRole('administrator') && $role->name != 'pracownik' && $role->name != 'administrator' && $role->name != 'użytkownik';
    }
}

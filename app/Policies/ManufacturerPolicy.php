<?php

namespace App\Policies;

use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ManufacturerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the manufacturers.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function view(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can view manufacturers.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function viewManufacturers(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can create manufacturers.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function create(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can update the manufacturers.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function update(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can delete the manufacturers.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function delete(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator');
    }
}

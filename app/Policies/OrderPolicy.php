<?php

namespace App\Policies;

use App\Entities\Order;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the orders.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function view(User $loggedUser, Order $order)
    {
        return ($loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik')) || ($loggedUser->hasRole('użytkownik') && $order->user->id == $loggedUser->id);
    }
    /**
     * Determine whether the user can view orders.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function viewOrders(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can create orders.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function create(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik') || $loggedUser->hasRole('użytkownik');
    }

    /**
     * Determine whether the user can update the orders.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function update(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can delete the orders.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function delete(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator');
    }
}

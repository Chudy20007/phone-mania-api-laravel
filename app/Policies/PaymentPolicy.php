<?php

namespace App\Policies;

use App\Entities\Payment;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the payments.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function view(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can view payments.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function viewPayments(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can create payments.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function create(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can update the payments.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function update(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can delete the payments.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function delete(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator');
    }
}

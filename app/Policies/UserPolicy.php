<?php

namespace App\Policies;

use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Entities\User  $loggedUser
     * @param  \App\Entities\User  $user
     * @return mixed
     */
    public function view(User $loggedUser, User $user)
    {
        return true;
    }
    public function viewUsers(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $loggedUser
     * @return mixed
     */
    public function create(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $loggedUser
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $loggedUser, User $user)
    {
        return ($loggedUser->hasRole('administrator') && !$user->hasRole('administrator')) || ($loggedUser->hasRole('pracownik') && $user->hasRole('użytkownik') || $loggedUser->id === $user->id);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $loggedUser
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $loggedUser, User $user)
    {
        return ($loggedUser->hasRole('administrator') && $loggedUser->id != $user->id && (!$user->hasRole('administrator'))) || ($loggedUser->hasRole('pracownik') && $user->hasRole('użytkownik') && $loggedUser->id != $user->id);
    }
}

<?php

namespace App\Policies;

use App\Entities\Processor;
use App\Entities\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProcessorPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the processors.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function view(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can view processors.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function viewProcessors(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }
    /**
     * Determine whether the user can create processors.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function create(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can update the processors.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function update(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator') || $loggedUser->hasRole('pracownik');
    }

    /**
     * Determine whether the user can delete the processors.
     *
     * @param  \App\Entities\User  $loggedUser
     * @return mixed
     */
    public function delete(User $loggedUser)
    {
        return $loggedUser->hasRole('administrator');
    }
}

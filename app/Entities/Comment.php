<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Comment.
 *
 * @package namespace App\Entities;
 */
class Comment extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['comment','user_id','product_id'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['id','product_id','user_id'];

    public function product()
    {
        return $this->belongsTo('App\Entities\Smartphone');
    }

    public function user()
    {
        return $this->belongsTo('App\Entities\User')->select(array('id','first_name', 'last_name'));
    }

}

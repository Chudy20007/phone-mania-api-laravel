<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Notifications\UserResetPasswordNotification;
use App\Notifications\OrderStatusNotification;
/**
 * Class User.
 *
 * @package namespace App\Entities;
 */
class User extends Authenticatable implements Transformable, JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    use TransformableTrait;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'deleted_at', 'updated_at', 'email', 'password', 'id', 'street', 'street_number', 'post_code','city'
    ];
    protected $hidden = ['id','password', 'remember_token','deleted_at'];


    public function sendOrderStatusAcceptedNotification($order,$status)
    {
        $this->notify(new OrderStatusNotification($this,$order,$status));
    }
    public function sendOrderStatusInProgress($order,$status)
    {
        $this->notify(new OrderStatusNotification($this,$order,$status));
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPasswordNotification($token));
    }
    public function sendActivationNotification($token)
    {
        $this->notify(new UserActivationNotification($token));
    }
    public function verifyUser()
    {
        return $this->hasOne('App\Entities\VerifyUser');
    }
    public function model()
    {
        return User::class;
    }

    public function roles()
    {
        return $this->belongsToMany('App\Entities\Role')->withTimestamps();
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [
            'email' => $this->email,
        ];
    }
    public function generateToken()
    {
        $access_token = str_random(60);
        return $access_token;
    }
    public function hasRole($roleName)
    {  
        foreach ($this->roles()->get() as $role) {
            if ($role->name == $roleName) {
                return true;
            }
        }
        return false;
    }

    public function orders()
    {
        return $this->belongsToMany('App\Entities\Order');
    }

    public function userComments()
    {
        return $this->hasMany('App\Entities\Comment');
    }

}

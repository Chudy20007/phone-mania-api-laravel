<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Ram.
 *
 * @package namespace App\Entities;
 */
class Ram extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','created_at','updated_at','size'];
    protected $dates = ['deleted_at'];
    protected $hidden =['deleted_at'];

}

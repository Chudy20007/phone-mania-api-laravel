<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Camera.
 *
 * @package namespace App\Entities;
 */
class Camera extends Model implements Transformable
{
    use SoftDeletes;
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','front','back','created_at','updated_at'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['deleted_at'];

    public function model()
    {
        return Camera::class;
    }
}

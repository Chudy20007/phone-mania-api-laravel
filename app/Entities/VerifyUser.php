<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class VerifyUser.
 *
 * @package namespace App\Entities;
 */
class VerifyUser extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable=['user_id','token'];
    public function user()
    {
        return $this->belongsTo('App\Entities\User', 'user_id');
    }
}

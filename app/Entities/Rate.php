<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Rate.
 *
 * @package namespace App\Entities;
 */
class Rate extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;
    use TransformableTrait;

    protected $fillable = ['rate','product_id','user_id'];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\Smartphone');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Memory.
 *
 * @package namespace App\Entities;
 */
class Memory extends Model implements Transformable
{
    use SoftDeletes;
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['size','created_at','updated_at'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['deleted_at'];

    public function model()
    {
        return Memory::class;
    }
}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Smartphone.
 *
 * @package namespace App\Entities;
 */
class Smartphone extends Model implements Transformable
{
    use SoftDeletes;
    use TransformableTrait;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'manufacturer_id',
        'display_id',
        'memory_id',
        'ram_id',
        'battery_id',
        'camera_id',
        'system_id',
        'main_photo',
        'resolution_id',
        'processor_id',
        'name',
        'count',
        'created_at',
        'updated_at',
        'price'
    ];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden = ['deleted_at','manufacturer_id',
    'display_id',
    'memory_id',
    'ram_id',
    'battery_id',
    'camera_id',
    'system_id',
    'main_photo',
    'resolution_id',
    'processor_id',];
    protected $fieldSearchable = [
        'name',
        'email',
        'manufacturer.name'=>'like',
        'display.id',
        'display.name'
    ];

    public function attachments()
    {
        return $this->belongsToMany('App\Entities\Attachment', 'smartphone_attachment','smartphone_id','attachment_id');
    }
    public function products()
    {
        return $this->belongsToMany('App\Entities\Order')->withPivot('count');
    }

    public function comments()
    {
        return $this->hasMany('App\Entities\Comment','product_id', 'id')->select(array('id','user_id','product_id','comment','created_at'))->orderBy('created_at','desc');
    }
    public function averageRating()
    {
        return round($this->hasMany('App\Entities\Rate', 'product_id', 'id')->avg('rate'), 2);
    }

    public function ratesCount()
    {
        return $this->hasMany('App\Entities\Rate', 'product_id', 'id')->count();
    }

    public function model()
    {
        return Smartphone::class;
    }
    public function manufacturer()
    {
        return $this->belongsTo('App\Entities\Manufacturer');
    }
    public function display()
    {
        return $this->belongsTo('App\Entities\Display');
    }

    public function memory()
    {
        return $this->belongsTo('App\Entities\Memory');
    }

    public function ram()
    {
        return $this->belongsTo('App\Entities\Ram');
    }

    public function battery()
    {
        return $this->belongsTo('App\Entities\Battery');
    }

    public function camera()
    {
        return $this->belongsTo('App\Entities\Camera');
    }

    public function resolution()
    {
        return $this->belongsTo('App\Entities\Resolution');
    }   

    public function processor()
    {
        return $this->belongsTo('App\Entities\Processor');
    } 
    
    public function system()
    {
        return $this->belongsTo('App\Entities\System');
    }     
}

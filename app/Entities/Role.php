<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Role.
 *
 * @package namespace App\Entities;
 */
class Role extends Model implements Transformable
{
    use SoftDeletes;
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','name','created_at','updated_at'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['deleted_at'];
    
    public function mode()
    {
        return Role::class;
    }
    public function role()
    {
        return $this->belongsTo('App\Entities\User','role_id','user_id')->get();
    }

}

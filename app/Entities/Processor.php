<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Processor.
 *
 * @package namespace App\Entities;
 */
class Processor extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['id','name','threads_info','threads_count','gpu_name'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['deleted_at'];
}

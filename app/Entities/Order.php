<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Notifications\Notifiable;
use App\Notifications\OrderStatusNotification;
/**
 * Class Order.
 *
 * @package namespace App\Entities;
 */
class Order extends Model implements Transformable
{
    use TransformableTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'tax', 'status', 'payment_id', 'total_price', 'discount'];
    protected $hidden = ['pivot'];
    

    public function products()
    {
        return $this->belongsToMany('App\Entities\Smartphone')->withPivot('count');
    }

    public function user()
    {
        return $this->belongsToMany('App\Entities\User');
    }

    public function payment()
    {
        return $this->belongsTo('App\Entities\Payment');
    }

    public function payment2()
    {
        return $this->belongsTo('App\Entities\Payment')->select(array('id','name'));
    }


}

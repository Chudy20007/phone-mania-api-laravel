<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VerifyUserRepository.
 *
 * @package namespace App\Repositories;
 */
interface VerifyUserRepository extends RepositoryInterface
{
    //
}

<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RAMRepository;
use App\Entities\Smartphone;
use App\Validators\SmartphoneValidator;

/**
 * Class SmartphoneRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SmartphoneRepositoryEloquent extends BaseRepository implements SmartphoneRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    protected $fieldSearchable = [
        'manufacturer.name' => 'like'
    ];
    public function model()
    {
        return Smartphone::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return SmartphoneValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}

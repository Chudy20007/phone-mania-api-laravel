<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\DisplayRepository;
use App\Entities\Display;
use App\Validators\DisplayValidator;

/**
 * Class DisplayRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class DisplayRepositoryEloquent extends BaseRepository implements DisplayRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Display::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return DisplayValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}

<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MemoryRepository.
 *
 * @package namespace App\Repositories;
 */
interface MemoryRepository extends RepositoryInterface
{
    //
}

<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MemoryRepository;
use App\Entities\Memory;
use App\Validators\MemoryValidator;

/**
 * Class MemoryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class MemoryRepositoryEloquent extends BaseRepository implements MemoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Memory::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return MemoryValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}

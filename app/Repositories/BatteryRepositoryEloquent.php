<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BatteryRepository;
use App\Entities\Battery;
use App\Validators\BatteryValidator;

/**
 * Class BatteryRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BatteryRepositoryEloquent extends BaseRepository implements BatteryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Battery::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return BatteryValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}

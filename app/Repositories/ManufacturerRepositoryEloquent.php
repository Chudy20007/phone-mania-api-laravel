<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ManufacturerRepository;
use App\Entities\Manufacturer;
use App\Validators\ManufacturerValidator;

/**
 * Class ManufacturerRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ManufacturerRepositoryEloquent extends BaseRepository implements ManufacturerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    protected $fieldSearchable = [
        'name' => "like"
    ];
    public function model()
    {
        return Manufacturer::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ManufacturerValidator::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    
}

<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SmartphoneRepository.
 *
 * @package namespace App\Repositories;
 */
interface SmartphoneRepository extends RepositoryInterface
{
    //
}

<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BatteryRepository.
 *
 * @package namespace App\Repositories;
 */
interface BatteryRepository extends RepositoryInterface
{
    //
}

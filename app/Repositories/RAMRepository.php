<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RAMRepository.
 *
 * @package namespace App\Repositories;
 */
interface RAMRepository extends RepositoryInterface
{
    //
}

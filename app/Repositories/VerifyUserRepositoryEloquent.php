<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\VerifyUserRepository;
use App\Entities\VerifyUser;
use App\Validators\VerifyUserValidator;

/**
 * Class VerifyUserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class VerifyUserRepositoryEloquent extends BaseRepository implements VerifyUserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return VerifyUser::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}

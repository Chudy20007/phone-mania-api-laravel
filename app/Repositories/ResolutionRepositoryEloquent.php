<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ResolutionRepository;
use App\Entities\Resolution;
use App\Validators\ResolutionValidator;

/**
 * Class ResolutionRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ResolutionRepositoryEloquent extends BaseRepository implements ResolutionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Resolution::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ResolutionValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}

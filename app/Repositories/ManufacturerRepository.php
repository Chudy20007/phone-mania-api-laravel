<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ManufacturerRepository.
 *
 * @package namespace App\Repositories;
 */
interface ManufacturerRepository extends RepositoryInterface
{
    //
}

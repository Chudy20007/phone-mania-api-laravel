<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RAMRepository;
use App\Entities\RAM;
use App\Validators\RAMValidator;

/**
 * Class RAMRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RAMRepositoryEloquent extends BaseRepository implements RAMRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RAM::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return RAMValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}

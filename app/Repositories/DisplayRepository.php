<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DisplayRepository.
 *
 * @package namespace App\Repositories;
 */
interface DisplayRepository extends RepositoryInterface
{
    //
}

<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProcessorRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProcessorRepository extends RepositoryInterface
{
    //
}

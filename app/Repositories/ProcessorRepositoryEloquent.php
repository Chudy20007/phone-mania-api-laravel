<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProcessorRepository;
use App\Entities\Processor;
use App\Validators\ProcessorValidator;

/**
 * Class ProcessorRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProcessorRepositoryEloquent extends BaseRepository implements ProcessorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Processor::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProcessorValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}

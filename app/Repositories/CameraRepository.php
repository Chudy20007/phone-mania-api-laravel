<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CameraRepository.
 *
 * @package namespace App\Repositories;
 */
interface CameraRepository extends RepositoryInterface
{
    //
}

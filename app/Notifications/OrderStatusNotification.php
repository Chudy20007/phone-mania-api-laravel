<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderStatusNotification extends Notification
{
    use Queueable;
    public $order;
    public $user;
    public $status;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user,$order, $status)
    {
        $this->user = $user;
        $this->order = $order;
        $this->status = $status;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    //Notifications sent via email
    public function via($notifiable)
    {
        return ['mail'];
    }
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $order = $this->order;
        $status = $this->status;
        $user = $this ->user;
        $header = array(
            'Content-Type' => 'application/json; charset=UTF-8',
            'charset' => 'utf-8',
        );
        return (new MailMessage)->from('rzuciak12@gmail.com', 'Phone Mania')->subject('Zmiana statusu zamówienia')
        ->view('email.order_accepted',array('data' => $this->makeMessage('Zamówienie zostało przyjęte!', null),'user'=>$user,'order'=>$order, 'status' => $status), $header);
    }

    private function makeMessage($message, $data)
    {
        return $response = [
            'message' => $message,
            //    'data' => $data->toArray(),
        ];
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class DisplayValidator.
 *
 * @package namespace App\Validators;
 */
class DisplayValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => ['required','regex:/^([A-ZĄĘŹŻŁÓŁa-ząęźżół]{1,}.[A-ZĄĘŹŻŁÓŁa-ząęźżół]{0,})$/'],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => ['required','regex:/^([A-ZĄĘŹŻŁÓŁa-ząęźżół]{1,}.[A-ZĄĘŹŻŁÓŁa-ząęźżół]{0,})$/'],
        ],
    ];
}

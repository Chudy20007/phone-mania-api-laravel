<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class SystemValidator.
 *
 * @package namespace App\Validators;
 */
class SystemValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃa-ząęćńźżółu]{3,})$/'],
            'version' => ['required', 'regex:/[A-ZĄĘŹŻŁÓŁa-ząęźżół0-9.]{3,}/']
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃa-ząęćńźżółu]{3,})$/'],
            'version' => ['required', 'regex:/[A-ZĄĘŹŻŁÓŁa-ząęźżół0-9.]{3,}/']
        ],
    ];
}

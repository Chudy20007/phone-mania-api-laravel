<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RateValidator.
 *
 * @package namespace App\Validators;
 */
class RateValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'rate' => ['required', 'regex:/^[+-]?([0-9]*[.])?[0-9]+$/'],
            'product_id' => ['required'],
            'user_id' => ['required']
        ],
        ValidatorInterface::RULE_UPDATE => [
            'rate' => ['required', 'regex:/^[+-]?([0-9]*[.])?[0-9]+$/'],
            'rate_id' => ['required'],
        ],
    ];
}

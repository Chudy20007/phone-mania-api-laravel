<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ResolutionValidator.
 *
 * @package namespace App\Validators;
 */
class ResolutionValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'width' => ['required','regex:/^([0-9]{3,5})$/'],
            'height' => ['required','regex:/^([0-9]{3,5})$/'] 
        ],
        ValidatorInterface::RULE_UPDATE => [
            'width' => ['required','regex:/^([0-9]{3,5})$/'],
            'height' => ['required','regex:/^([0-9]{3,5})$/']
        ],
    ];
}

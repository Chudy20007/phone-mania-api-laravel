<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class PaymentValidator.
 *
 * @package namespace App\Validators;
 */
class PaymentValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => ['name' => ['required', 'regex:/[A-Za-ząęóźżćłĄĘŹŻĆŁÓ ]{4,}/']],
        ValidatorInterface::RULE_UPDATE => ['name' => ['required', 'regex:/[A-Za-ząęóźżćłĄĘŹŻĆŁÓ ]{4,}/']],
    ];
}

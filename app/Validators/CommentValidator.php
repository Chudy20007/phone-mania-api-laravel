<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class CommentValidator.
 *
 * @package namespace App\Validators;
 */
class CommentValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'comment' => ['required'],
            'product_id' => ['required','regex:/^\d+$/'],
            'user_id' => ['required','regex:/^\d+$/']
        ],
        ValidatorInterface::RULE_UPDATE => [
            'comment' => ['required'],
            'user_id' => ['required']
        ],
    ];
}

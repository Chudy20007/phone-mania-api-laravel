<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class ManufacturerValidator.
 *
 * @package namespace App\Validators;
 */
class ManufacturerValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => ['required','regex:/^([A-ZĄĘĆŹŻŁŃa-z ]{2,})$/']
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => ['required','regex:/^([A-ZĄĘĆŹŻŁŃa-z ]{2,})$/']
        ],
    ];
}

<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RoleValidator.
 *
 * @package namespace App\Validators;
 */
class RoleValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => ['required', 'regex:/^([a-ząęćńźżółu]{3,})$/']
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => ['required', 'regex:/^([a-ząęćńźżółu]{3,})$/']
        ],
    ];
}

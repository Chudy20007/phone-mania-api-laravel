<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class BatteryValidator.
 *
 * @package namespace App\Validators;
 */
class BatteryValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
        'type' => ['required', 'regex:/^([A-Z]{1}[a-z]{3,}[a-z]-[a-z]{3,})$/'],
        'capacity' => ['required', 'regex:/^([0-9]{4,})$/'],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'type' => ['required', 'regex:/^([A-Z]{1}[a-z]{3,}[a-z]-[a-z]{3,})$/'],
            'capacity' => ['required', 'regex:/^([0-9]{4,})$/'],
        ],
    ];
}

<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class RamValidator.
 *
 * @package namespace App\Validators;
 */
class RamValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => ['size' => ['required','regex:/^([0-9]{1,2})$/']],
        ValidatorInterface::RULE_UPDATE => ['size' => ['required','regex:/^([0-9]{1,2})$/']],
    ];
}

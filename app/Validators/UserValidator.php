<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class UserValidator.
 *
 * @package namespace App\Validators;
 */
class UserValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'first_name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/'],
            'last_name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/'],
            'email' => ['required','regex:/^[0-9a-zA-Z_.-]+@[0-9a-zA-Z.-]+\.[a-zA-Z]{2,3}$/', 'email'],
            'street' => ['required','regex:/[A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{2,}/'],
            'street_number' => ['required', 'regex:/[A-ZĄĘĆŹŻŁŃa-ząęćńźżółu0-9]{2,}/'],
            'post_code' => ['required', 'regex:/^([0-9]{2})(-[0-9]{3})$/'],
            'city' => ['required', 'regex:/([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})/'],
            'password' => ['required'],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'first_name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/'],
            'last_name' => ['required', 'regex:/^([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})$/'],
            'email' => ['required', 'email'],
            'street' => ['required','regex:/[A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{2,}/'],
            'street_number' => ['required', 'regex:/[A-ZĄĘĆŹŻŁŃa-ząęćńźżółu0-9]{2,}/'],
            'post_code' => ['required', 'regex:/^([0-9]{2})(-[0-9]{3})$/'],
            'city' => ['required', 'regex:/([A-ZĄĘĆŹŻŁŃ]{1}[a-ząęćńźżółu]{3,})/'],
            'password' => ['required']
        ],
    ];
}

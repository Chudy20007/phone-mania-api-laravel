<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class CameraValidator.
 *
 * @package namespace App\Validators;
 */
class CameraValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'front' => ['required', 'regex:/^([0-9A-ZĄĘŹŻŁÓŁa-ząęźżół ]{2,})$/'],
            'back' => ['required', 'regex:/^([0-9A-ZĄĘŹŻŁÓŁa-ząęźżół ]{2,})$/']
        ],
        ValidatorInterface::RULE_UPDATE => [
            'front' => ['required', 'regex:/^([0-9A-ZĄĘŹŻŁÓŁa-ząęźżół]{2,})$/'],
            'back' => ['required', 'regex:/^([0-9A-ZĄĘŹŻŁÓŁa-ząęźżół]{2,})$/']
        ],
    ];
}

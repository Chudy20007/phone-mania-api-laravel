<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class MemoryValidator.
 *
 * @package namespace App\Validators;
 */
class MemoryValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'size' => ['required','regex:/^([0-9]{1,4})$/'] 
        ],
        ValidatorInterface::RULE_UPDATE => [
            'size' => ['required','regex:/^([0-9]{1,4})$/'] 
        ],
    ];
}

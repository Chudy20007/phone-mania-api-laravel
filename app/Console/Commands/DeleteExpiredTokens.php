<?php

namespace App\Console\Commands;
use App\Entities\VerifyUser;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteExpiredTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tokens:delete-expired-tokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete expired tokens (7days)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->processDel();
        } catch (\Exception $e) {
            $message = sprintf($e);
            Log::debug($message);
            $this->error($message);
        }
    }

    private function processDel()
    {
        VerifyUser::where('created_at', '<', Carbon::now()->subDays(7))->delete();
    }
    
}

<?php

namespace App\Providers;
use App\Entities\User;
use App\Entities\Display;
use App\Entities\Role;
use App\Entities\Ram;
use App\Entities\Memory;
use App\Entities\Processor;
use App\Entities\Smartphone;
use App\Entities\Order;
use App\Entities\Resolution;
use App\Entities\System;
use App\Entities\Camera;
use App\Entities\Battery;
use App\Entities\Manufacturer;
use App\Policies\DisplayPolicy;
use App\Policies\RolePolicy;
use App\Policies\RamPolicy;
use App\Policies\UserPolicy;
use App\Policies\MemoryPolicy;
use App\Policies\ProcessorPolicy;
use App\Policies\SmartphonePolicy;
use App\Policies\OrderPolicy;
use App\Policies\ResolutionPolicy;
use App\Policies\SystemPolicy;
use App\Policies\CameraPolicy;
use App\Policies\BatteryPolicy;
use App\Policies\ManufacturerPolicy;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Display::class => DisplayPolicy::class,
        User::class => UserPolicy::class,
        Role::class => RolePolicy::class,
        Resolution::class => ResolutionPolicy::class,
        Ram::class => RamPolicy::class,
        Memory::class => MemoryPolicy::class,
        Manufacturer::class => ManufacturerPolicy::class,
        Camera::class => CameraPolicy::class,
        Battery::class => BatteryPolicy::class,
        System::class => SystemPolicy::class,
        Smartphone::class => SmartphonePolicy::class,
        Order::class => OrderPolicy::class,
        Processor::class => ProcessorPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}

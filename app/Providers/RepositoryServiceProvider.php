<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RoleRepository::class, \App\Repositories\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ArticleRepository::class, \App\Repositories\ArticleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SmartphoneRepository::class, \App\Repositories\SmartphoneRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TagRepository::class, \App\Repositories\TagRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ManufacturerRepository::class, \App\Repositories\ManufacturerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BatteryRepository::class, \App\Repositories\BatteryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DisplayRepository::class, \App\Repositories\DisplayRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ResolutionRepository::class, \App\Repositories\ResolutionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MemoryRepository::class, \App\Repositories\MemoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RAMRepository::class, \App\Repositories\RAMRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CameraRepository::class, \App\Repositories\CameraRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProcessorRepository::class, \App\Repositories\ProcessorRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SystemRepository::class, \App\Repositories\SystemRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\VerifyUserRepository::class, \App\Repositories\VerifyUserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\OrderRepository::class, \App\Repositories\OrderRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PaymentRepository::class, \App\Repositories\PaymentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AttachmentRepository::class, \App\Repositories\AttachmentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CommentRepository::class, \App\Repositories\CommentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\RateRepository::class, \App\Repositories\RateRepositoryEloquent::class);
        //:end-bindings:
    }
}

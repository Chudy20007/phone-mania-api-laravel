<?php

Route::group([
	'middleware' =>['jwt.auth','jwt.check','basic.permissions'],
],function($route){
	Route::get('user-orders','OrdersController@userOrders');
	Route::post('rates','RatesController@store');
	Route::get('/authenticate/refresh', 'AuthenticateController@refreshToken');
	Route::resource('orders', 'OrdersController',['include' => ['store','show']]);
});

Route::group([
		'middleware' =>['jwt.auth','jwt.check', 'extended.permissions'],
], function ($route) {
	Route::resource('rates','RatesController',['except'=> ['store']]);
	Route::resource('smartphones','SmartphonesController',['except'=> ['index','show']]);
	Route::resource('manufacturers', 'ManufacturersController',['except' => ['index']]);
	Route::resource('orders', 'OrdersController',['except' => ['store','show']]);
	Route::put('orders/{id}/update-status', 'OrdersController@updateOrderStatus');
	Route::resources( [
		'users' => 'UsersController',
		'roles' => 'RolesController',
		'memories' => 'MemoriesController',
		'tags' => 'TagsController',
		'cameras' => 'CamerasController',
		'rams' => 'RamsController',
		'resolutions' => 'ResolutionsController',
		'displays' => 'DisplaysController',
		'batteries' => 'BatteriesController',
		'processors' => 'ProcessorsController',
		'systems' => 'SystemsController',
		'payments' => 'PaymentsController',
	]);

Route::delete('orders/{orderID}/delete/{productID}','OrdersController@removeProductFromOrder');
});

Route::group(['middleware' => ['jwt.auth', 'jwt.refresh']], function() {
	Route::post('logout', 'AuthController@logout');
});

Route::get('smartphones/{id}','SmartphonesController@show');
Route::get('manufacturers','ManufacturersController@index');
Route::post('find-products-by-manufacturers', 'SmartphonesController@findProductsByManufacturers');
Route::get('smartphones','SmartphonesController@index');
Route::get('login', [ 'as' => 'login', 'uses' => 'AuthController@login']);
Route::post('login', 'AuthController@login');


Route::get('find/{value}','SmartphonesController@findProducts');
Route::get('storage/app/public/img/photos/{img}', 'SmartphonesController@getPhotos');
Route::post('refresh', 'AuthController@refresh');
Route::get('/user/verify/{token}', 'UsersController@verifyUser');
Route::get('reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('reset', 'Auth\ResetPasswordController@reset');
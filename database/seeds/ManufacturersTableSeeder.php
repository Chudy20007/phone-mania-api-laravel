<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ManufacturersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\ManufacturerRepository $repository)
    {
        $repository->create([
            'name'  => 'Samsung',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Huawei',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Nokia',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]); 

        $repository->create([
            'name'  => 'Xiaomi',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Motorola',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'LG',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]); 

        $repository->create([
            'name'  => 'Apple',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]); 
    }
}

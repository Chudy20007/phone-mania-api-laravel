<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProcessorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\ProcessorRepository $repository)
    {
        $repository->create([
            'name'  => 'Qualcomm Snapdragon 425',
            'threads_info' => '4 rdzenie, 1.40 GHz, Cortex A53',
            'threads_count' => '4',
            'gpu_name' => 'Adreno 308',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Qualcomm Snapdragon 430',
            'threads_info' => '4 rdzenie, 1.40 GHz, A53 + 4 rdzenie, 1.1 GHz, A53',
            'threads_count' => '8',
            'gpu_name' => 'Adreno 308',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Qualcomm Snapdragon 625',
            'threads_info' => '8 rdzeni, 2.0 GHz, Cortex A53',
            'threads_count' => '8',
            'gpu_name' => 'Adreno 506',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Qualcomm Snapdragon 821',
            'threads_info' => '2 rdzenie, 2.35 GHz, Kryo + 2 rdzenie, 1.6 GHz, Kryo',
            'threads_count' => '4',
            'gpu_name' => 'Adreno 530',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'MediaTek MT6750',
            'threads_info' => '4 rdzenie, 1.5 GHz, Cortex A53+4 rdzenie, 1.0 GHz, Cortex A53',
            'threads_count' => '8',
            'gpu_name' => 'Mali-T860',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Qualcomm Snapdragon 636',
            'threads_info' => '4 rdzenie, 1,80 GHz, Kryo + 4 rdzenie, 1.60 GHz, Kryo',
            'threads_count' => '8',
            'gpu_name' => 'Adreno 509',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Apple A9 z koprocesorem M9',
            'threads_info' => '2 rdzenie, 1.85 GHz',
            'threads_count' => '2',
            'gpu_name' => 'PowerVR GT7600',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Apple A10 z koprocesorem M10',
            'threads_info' => '4 rdzenie, 2.34 GHz',
            'threads_count' => '4',
            'gpu_name' => 'Mali-T860',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Apple A11 Bionic z koprocesorem M11',
            'threads_info' => '2 rdzenie, 2.39 Ghz Monsoon + 4 rdzenie, 1.30 - 1.70 GHz, Mistral',
            'threads_count' => '6',
            'gpu_name' => 'PowerVR',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

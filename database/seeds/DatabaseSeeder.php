<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(UserRolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(SystemsTableSeeder::class);
        $this->call(CamerasTableSeeder::class);
        $this->call(ManufacturersTableSeeder::class);
        $this->call(RamsTableSeeder::class);
        $this->call(MemoriesTableSeeder::class);
        $this->call(BatteriesTableSeeder::class);
        $this->call(ProcessorsTableSeeder::class);
        $this->call(DisplaysTableSeeder::class);
        $this->call(ResolutionsTableSeeder::class);
       // $this->call(SmartphonesTableSeeder::class);
      //  $this->call(RateTableSeeder::class);
    }
}

<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\TagRepository $repository)
    {
        $repository->create([
            'name' => 'Android',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'name' => 'iOS',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

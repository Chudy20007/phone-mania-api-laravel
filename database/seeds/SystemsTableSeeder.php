<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SystemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\SystemRepository $repository)
    {
        $repository->create([
            'name'  => 'Android',
            'version' => '4.4.4 KitKat',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'name'  => 'Android',
            'version' => '5.1.1 Lollipop',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Android',
            'version' => '6.0.1 Marshmallow',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Android',
            'version' => '7.1.2 Nougat',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'Android',
            'version' => '8.0 Oreo',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'iOS',
            'version' => '10.3',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'iOS',
            'version' =>'12.0',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'iOS',
            'version' => '11.4.1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

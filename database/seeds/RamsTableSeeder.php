<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\RamRepository $repository)
    {
        $repository->create([
            'size'  => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '2',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '3',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '4',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '6',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '8',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

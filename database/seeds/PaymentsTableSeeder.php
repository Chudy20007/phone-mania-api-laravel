<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\PaymentRepository $repository)
    {
        $repository->create([
            'name'  => 'gotówka przy odbiorze',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'karta kredytowa',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'przelew',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

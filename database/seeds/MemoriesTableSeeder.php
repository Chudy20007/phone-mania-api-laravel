<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MemoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\MemoryRepository $repository)
    {
        $repository->create([
            'size'  => '8',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '16',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '32',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '64',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '128',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $repository->create([
            'size'  => '256',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

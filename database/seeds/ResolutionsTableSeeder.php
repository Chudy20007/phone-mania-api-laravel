<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ResolutionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\ResolutionRepository $repository)
    {
        $repository->create([
            'width'  => '2960',
            'height' => '1440',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'width'  => '2160',
            'height' => '1080',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'width'  => '2560',
            'height' => '1440',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'width'  => '1920',
            'height' => '1080',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'width'  => '1280',
            'height' => '720',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'width'  => '1440',
            'height' => '720',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

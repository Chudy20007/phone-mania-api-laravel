<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SmartphonesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\SmartphoneRepository $repository)
    {
        $repository->create([
            'name'  => 'Galaxy A6',
            'price' => '1140',
            'count' => 50,
            'manufacturer_id' => 1,
            'display_id' => 2,
            'ram_id' => 2,
            'resolution_id' => 2,
            'memory_id' => 1,
            'camera_id' => 1,
            'system_id' => 4,
            'processor_id' => 2,
            'battery_id' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'name'  => 'P20 Pro',
            'price' => '1540',
            'count' => 50,
            'manufacturer_id' => 2,
            'display_id' => 2,
            'ram_id' => 3,
            'resolution_id' => 3,
            'memory_id' => 4,
            'camera_id' => 2,
            'system_id' => 4,
            'processor_id' => 3,
            'battery_id' => 3,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

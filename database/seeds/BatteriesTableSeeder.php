<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BatteriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\BatteryRepository $repository)
    {
        $repository->create([
            'capacity'  => '3000',
            'type' => 'Litowo-jonowa',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'capacity'  => '3100',
            'type' => 'Litowo-polimerowa',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'capacity'  => '2200',
            'type' => 'Litowo-jonowa',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'capacity'  => '2800',
            'type' => 'Litowo-jonowa',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'capacity'  => '3300',
            'type' => 'Litowo-polimerowa',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'capacity'  => '2600',
            'type' => 'Litowo-jonowa',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

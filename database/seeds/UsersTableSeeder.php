<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\UserRepository $repository)
    {
      $u =  $repository->create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'anonim410@gmail.com',
            'street' => 'Krzywa',
            'street_number' => '45',
            'city' => 'Warszawa',
            'post_code' => '21-210',
            'password' => bcrypt('admin1'),
            'verified' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $u->roles()->attach(3);
       $u =  $repository->create([
            'first_name' => 'Super',
            'last_name' => 'Adminn',
            'email' => 'admin2@wp.pl',
            'street' => 'Bliska',
            'street_number' => '15',
            'city' => 'Kielce',
            'post_code' => '21-219',
            'password' => bcrypt('admin2'),
            'verified' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
        $u->roles()->attach(1);

        $faker = Faker::create();
    	foreach (range(1,10) as $index) {
	        DB::table('users')->insert([
                'first_name' => 'Super',
                'last_name' => 'Adminn',
                'email' => $faker->email,
                'street' => 'Bliska',
                'street_number' => '15',
                'city' => 'Kielce',
                'post_code' => '21-219',
                'password' => bcrypt('admin2'),
                'verified' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
	        ]);
	}
    }
}

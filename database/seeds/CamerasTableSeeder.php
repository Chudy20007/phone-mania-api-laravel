<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CamerasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\App\Repositories\CameraRepository $repository)
    {
        $repository->create([
            'front'  => '8 MP',
            'back' => '13 MP',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);

        $repository->create([
            'front'  => '10 MP',
            'back' => '18 MP',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),

        ]);
    }
}

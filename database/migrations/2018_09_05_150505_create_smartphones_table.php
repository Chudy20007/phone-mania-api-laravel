<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateSmartphonesTable.
 */
class CreateSmartphonesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('smartphones', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('communication')->nullable();
			$table->string('price');
			$table->string('photo')->nullable();
			$table->integer('manufacturer_id')->unsigned();
			$table->integer('display_id')->unsigned();
			$table->integer('camera_id')->unsigned();
			$table->integer('processor_id')->unsigned();
			$table->integer('system_id')->unsigned();
			$table->integer('battery_id')->unsigned();
			$table->integer('ram_id')->unsigned();
			$table->integer('memory_id')->unsigned();
			$table->integer('resolution_id')->unsigned();;
			$table->integer('count');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('manufacturer_id')
			->references('id')
			->on('manufacturers');
			$table->foreign('display_id')
			->references('id')
			->on('displays');
			$table->foreign('camera_id')
			->references('id')
			->on('cameras');
			$table->foreign('battery_id')
			->references('id')
			->on('batteries');
			$table->foreign('ram_id')
			->references('id')
			->on('rams');
			$table->foreign('processor_id')
			->references('id')
			->on('processors');
			$table->foreign('memory_id')
			->references('id')
			->on('memories');
			$table->foreign('resolution_id')
			->references('id')
			->on('resolutions');
			$table->foreign('system_id')
			->references('id')
			->on('systems');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::drop('smartphones');
	}
}

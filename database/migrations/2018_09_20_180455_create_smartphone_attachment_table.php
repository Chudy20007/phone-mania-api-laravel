<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmartphoneAttachmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('smartphone_attachment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attachment_id')->unsigned();
            $table->integer('smartphone_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('attachment_id')
                ->references('id')
                ->on('attachments');
            $table->foreign('smartphone_id')
                ->references('id')
                ->on('smartphones')->onDelete('cascade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    { Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('smartphone_attachment');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateProcessorsTable.
 */
class CreateProcessorsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('processors', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('threads_info');
			$table->integer('threads_count');
			$table->string('gpu_name');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::disableForeignKeyConstraints();
		Schema::drop('processors');
	}
}

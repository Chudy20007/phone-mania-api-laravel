<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class ProcessorTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\ProcessorRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetProcessorsList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/processors',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }
    public function testGetFailedProcessorsList()
    {

        $response = $this->json('GET', '/processors', ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
        
    }
    public function testGetProcessor()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/processors/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetProcessor()
    {
        $response = $this->json('GET', '/processors/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeleteProcessor()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/processors/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteProcessor()
    {
        $response = $this->json('DELETE', '/processors/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateProcessorValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $processor = [
            'name' => 'Qualcomm Snapdragon 420',
            'threads_info' => '8',
            'threads_count' => 'T',
            'gpu_name' => ''
        ];
        $response = $this->json('PUT', '/processors/2',$processor, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateProcessor()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $processor = [
            'name' => 'Qualcomm Snapdragon 450',
            'threads_info' => '8 rdzeni 1.80 GHz, A53',
            'threads_count' => 8,
            'gpu_name' => 'Adreno 308'
        ];
        $response = $this->json('PUT', '/processors/2',$processor, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedUpdateProcessor()
    {
        $processor = [
            'name' => 'Qualcomm Snapdragon 450',
            'threads_info' => '8 rdzeni 1.80 GHz, A53',
            'threads_count' => 8,
            'gpu_name' => 'Adreno 308'
        ];
        $response = $this->json('PUT', '/processors/2',$processor,[null],['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
  
    public function testAddProcessorValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $processor = [
            'name' => 'Qualcomm Snapdragon 425',
            'threads_info' => '8',
            'threads_count' => '',
            'gpu_name' => '308'
        ];
        $response = $this->json('POST', '/processors',$processor, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testAddProcessor()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $processor = [
            'name' => 'Qualcomm Snapdragon 450',
            'threads_info' => '8 rdzeni 1.80 GHz, A53',
            'threads_count' => 8,
            'gpu_name' => 'Adreno 308'
        ];
        $response = $this->json('POST', '/processors',$processor, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testAddFailedProcessor()
    {
        $processor = [
            'name' => 'Qualcomm Snapdragon 450',
            'threads_info' => '8 rdzeni 1.80 GHz, A53',
            'threads_count' => 8,
            'gpu_name' => 'Adreno 308'
        ];
        $response = $this->json('POST', '/processors', $processor,[null], ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

}

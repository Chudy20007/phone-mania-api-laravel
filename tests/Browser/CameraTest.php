<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class CameraTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\CameraRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetCamerasList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/cameras',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }

    public function testGetCamera()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/cameras/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetCamera()
    {
        $response = $this->json('GET', '/cameras/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeleteCamera()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/cameras/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteCamera()
    {
        $response = $this->json('DELETE', '/cameras/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateCameraValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $camera = [
            'id' => 2,
            'front' => 'X',
            'back' => ''
        ];
        $response = $this->json('PUT', '/cameras/2',$camera, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateCamera()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $camera = [
            'id' => 2,
            'front' => '13',
            'back' => '13'
        ];
        $response = $this->json('PUT', '/cameras/2',$camera, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testFailedUpdateCamera()
    {
        $response = $this->json('PUT', '/cameras/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
  
    public function testAddCameraValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $camera = [
            'front' => 12,
            'back' => 'X'
        ];
        $response = $this->json('POST', '/cameras',$camera, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testAddCamera()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $camera = [
            'front' => '13',
            'back' => '13'
        ];
        $response = $this->json('POST', '/cameras',$camera, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testAddFailedCamera()
    {
        $camera = [
            'front' => '13',
            'back' => '13'
        ];
        $response = $this->json('POST', '/cameras',$camera,[null], ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

}

<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class OrderTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\OrderRepository');

        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetOrdersList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/orders', [null], $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);

    }
    public function testGetOrdersListAsGuest()
    {
        $response = $this->json('GET', '/orders', ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }
    public function testGetOrder()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/orders/2', [null], $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }
    public function testFailedGetOrder()
    {
        $response = $this->json('GET', '/orders/2')
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }
    public function testDeleteOrder()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/orders/2', [null], $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteOrder()
    {
        $response = $this->json('DELETE', '/orders/2')
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }

    public function testUpdateOrderValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $order = [
            'payment_id' => 12,
            'total_price' => 'x5100.582',
            'user_id' => 'x',
            'products' => [
                '0' => [
                    'id' => 1,
                ],
            ],
        ];
        $response = $this->json('PUT', '/orders/2', $order, $headers, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJsonStructure(['message']);
    }

    public function testUpdateOrder()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $order = [
            'payment_id' => 2,
            'total_price' => 5100.58,
            'user_id' => 1,
            'products' => [
                '0' => [
                    'id' => 1,
                ],
            ],
        ];
        $response = $this->json('PUT', '/orders/2', $order, $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }
    public function testFailedUpdateOrder()
    {
        $response = $this->json('PUT', '/orders/2')
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }

    public function testAddOrderValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $order = [
            'payment_id' => 'x',
            'total_price' => '',
            'user_id' => 1,
            'products' => [
            ],
        ];
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('POST', '/orders', $order, $headers, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJsonStructure(['message']);
    }

    public function testAddOrder()
    {
        $token = JWTAuth::attempt($this->credentials);
        $order = [
            'payment_id' => 2,
            'total_price' => 5100.58,
            'user_id' => 1,
            'products' => [
                '0' => [
                    'id' => 1,
                    'count' => 2,
                ],
                '1' => [
                    'id' => 2,
                    'count' => 2,
                ],
            ],
        ];
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('POST', '/orders', $order, $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }
    public function testFailedAddOrder()
    {
        $order = [
            'payment_id' => 2,
            'total_price' => 5100.58,
            'user_id' => 1,
            'products' => [
                '0' => [
                    'id' => 1,
                    'count' => 2,
                ],
                '1' => [
                    'id' => 2,
                    'count' => 2,
                ],
            ],
        ];
        $response = $this->json('POST', '/orders', $order, [null], ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }
}

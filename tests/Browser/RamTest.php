<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class RamTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\RAMRepository');

        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetRamsList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/rams', [null], $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);

    }
    public function testGetFailedRamsList()
    {

        $response = $this->json('GET', '/rams', ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJsonStructure(['message']);

    }
    public function testGetRam()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/rams/2', [null], $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }
    public function testFailedGetRam()
    {
        $response = $this->json('GET', '/rams/2')
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }
    public function testDeleteRam()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/rams/6', [null], $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteRam()
    {
        $response = $this->json('DELETE', '/rams/6')
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }

    public function testUpdateRamValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $memory = [
            'id' => 2,
            'size' => 1,
        ];
        $response = $this->json('PUT', '/rams/2', $memory, $headers, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJsonStructure(['message']);
    }

    public function testUpdateRam()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $memory = [
            'id' => 2,
            'size' => 16,
        ];
        $response = $this->json('PUT', '/rams/2', $memory, $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }
    public function testFailedUpdateRam()
    {
        $memory = [
            'size' => 16,
        ];
        $response = $this->json('PUT', '/rams/2', $memory, [null], ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }
    public function testAddRamValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $memory = [
            'size' => 'example',
        ];
        $response = $this->json('POST', '/rams', $memory, $headers, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJsonStructure(['message']);
    }
    public function testAddRam()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $memory = [
            'size' => 16,
        ];
        $response = $this->json('POST', '/rams', $memory, $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }
    public function testAddFailedRam()
    {
        $memory = [
            'size' => 16,
        ];
        $response = $this->json('POST', '/rams', $memory, [null], ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }

}

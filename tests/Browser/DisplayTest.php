<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class DisplayTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\DisplayRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetDisplaysList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/displays',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }
    public function testGetFailedDisplaysList()
    {

        $response = $this->json('GET', '/displays', ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
        
    }
    public function testGetDisplay()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/displays/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetDisplay()
    {
        $response = $this->json('GET', '/displays/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeleteDisplay()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/displays/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteDisplay()
    {
        $response = $this->json('DELETE', '/displays/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateDisplayValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $display = [
            'id' => 2,
            'name' => 'OLED'
        ];
        $response = $this->json('PUT', '/displays/2',$display, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateDisplay()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $display = [
            'id' => 2,
            'name' => 'TFT'
        ];
        $response = $this->json('PUT', '/displays/2',$display, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedUpdateDisplay()
    {
        $response = $this->json('PUT', '/displays/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
   
    public function testAddDisplayValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $display = [
            'name' => '123'
        ];
        $response = $this->json('POST', '/displays',$display, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testAddDisplay()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $display = [
            'name' => 'TFT'
        ];
        $response = $this->json('POST', '/displays',$display, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testAddFailedDisplay()
    {
        $display = [
            'name' => 'TFT'
        ];
        $response = $this->json('POST', '/displays', $display,[null], ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

}

<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class RoleTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\RoleRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetRolesList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/roles',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }
    public function testGetFailedRolesList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/roles', ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testGetRole()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/roles/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetRole()
    {
        $response = $this->json('GET', '/roles/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeleteRole()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/roles/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteRole()
    {
        $response = $this->json('DELETE', '/roles/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateUser()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $role = [
            'id' => 2,
            'name' => 'redaktor'
        ];
        $response = $this->json('PUT', '/roles/2',$role, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testUpdateUserValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $role = [
            'id' => 2,
            'name' => '12'
        ];
        $response = $this->json('PUT', '/roles/2',$role, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }
    public function testFailedUpdateRole()
    {
        $response = $this->json('PUT', '/roles/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function addRoleValidation()
    {
        $role = [
            'name' => 'administrator'
        ];
        $response = $this->json('POST', '/roles',$role, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }  
    public function addRole()
    {
        $role = [
            'name' => 'redaktor'
        ];
        $response = $this->json('POST', '/roles',$role, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }

}

<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class PaymentTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\PaymentRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetPaymentsList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/payments',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }
    public function testGetFailedPaymentsList()
    {

        $response = $this->json('GET', '/payments', ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
        
    }
    public function testGetPayment()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/payments/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetPayment()
    {
        $response = $this->json('GET', '/payments/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeletePayment()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/payments/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeletePayment()
    {
        $response = $this->json('DELETE', '/payments/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testUpdatePaymentValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $payment = [
            'name' => 'przelew'
        ];
        $response = $this->json('PUT', '/payments/2',$payment, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }
    public function testUpdatePayment()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $payment = [
            'name' => 'test'
        ];
        $response = $this->json('PUT', '/payments/2',$payment, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedUpdatePayment()
    {
        $payment = [
            'name' => 'test'
        ];
        $response = $this->json('PUT', '/payments/2',$payment,[null],['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testAddPaymentValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $payment = [
            'name' => '1'
        ];
        $response = $this->json('POST', '/payments',$payment, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    } 
    public function testAddPayment()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $payment = [
            'name' => 'test'
        ];
        $response = $this->json('POST', '/payments',$payment, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testAddFailedPayment()
    {
        $payment = [
            'name' => 'test'
        ];
        $response = $this->json('POST', '/payments', $payment,[null], ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

}

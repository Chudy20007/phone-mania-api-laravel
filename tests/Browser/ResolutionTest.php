<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class ResolutionTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\ResolutionRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetResolutionsList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/resolutions',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }
    public function testGetFailedResolutionsList()
    {

        $response = $this->json('GET', '/resolutions', ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
        
    }
    public function testGetResolution()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/resolutions/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetResolution()
    {
        $response = $this->json('GET', '/resolutions/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeleteResolution()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/resolutions/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteResolution()
    {
        $response = $this->json('DELETE', '/resolutions/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateResolution()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $resolution = [
            'width' => '720',
            'height' => '1266'
        ];
        $response = $this->json('PUT', '/resolutions/2',$resolution, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testUpdateResolutionValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $resolution = [
            'width' => 'test',
            'height' => '1'
        ];
        $response = $this->json('PUT', '/resolutions/2',$resolution, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }
    public function testFailedUpdateResolution()
    {
        $resolution = [
            'width' => '720',
            'height' => '1266'
        ];
        $response = $this->json('PUT', '/resolutions/2',$resolution,[null],['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testAddResolutionValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $resolution = [
            'width' => 'x',
            'height' => 'x'
        ];
        $response = $this->json('POST', '/resolutions',$resolution, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testAddResolution()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $resolution = [
            'width' => '720',
            'height' => '1266'
        ];
        $response = $this->json('POST', '/resolutions',$resolution, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testAddFailedResolution()
    {
        $resolution = [
            'width' => '720',
            'height' => '1266'
        ];
        $response = $this->json('POST', '/resolutions', $resolution,[null], ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

}

<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use JWTAuth;
use Tests\DuskTestCase;

class SmartphoneTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\SmartphoneRepository');

        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetSmartphonesList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/smartphones', [null], $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);

    }
    public function testGetSmartphonesListAsGuest()
    {
        $response = $this->json('GET', '/smartphones', ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }
    public function testGetSmartphone()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/smartphones/2', [null], $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }
    public function testGetSmartphoneAsGuest()
    {
        $response = $this->json('GET', '/smartphones/2')
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }
    public function testDeleteSmartphone()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/smartphones/2', [null], $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteSmartphone()
    {
        $response = $this->json('DELETE', '/smartphones/2')
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }

    public function testUpdateSmartphone()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $smartphone = [
            'name' => 'Maxi 2',
            'price' => 5100,
            'ram_id' => 1,
            'memory_id' => 2,
            'display_id' => 1,
            'resolution_id' => 2,
            'battery_id' => 2,
            'camera_id' => 1,
            'manufacturer_id' => 3,
            'system_id' => 4,
            'processor_id' => 3,
            'count' => 100,
        ];
        $response = $this->json('PUT', '/smartphones/2', $smartphone, $headers, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data']);
    }

    public function testUpdateSmartphoneValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $smartphone = [
            'name' => 'xxadasd',
            'price' => 0.08789,
            'ram_id' => 'a',
            'memory_id' => '',
            'display_id' => 100,
            'resolution_id' => 20,
            'battery_id' => 'x',
            'camera_id' => 'select',
            'system_id' => 4,
            'processor_id' => 3,
            'count' => true,
        ];
        $response = $this->json('PUT', '/smartphones/2', $smartphone, $headers, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJsonStructure(['message']);
    }

    public function testFailedUpdateSmartphone()
    {
        $response = $this->json('PUT', '/smartphones/2')
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }

    public function testAddSmartphone()
    {
        $token = JWTAuth::attempt($this->credentials);
        $smartphone = [
            'name' => 'Maxi 2',
            'price' => 5100,
            'ram_id' => 1,
            'memory_id' => 2,
            'display_id' => 1,
            'resolution_id' => 2,
            'battery_id' => 2,
            'camera_id' => 1,
            'manufacturer_id' => 3,
            'system_id' => 4,
            'processor_id' => 3,
            'count' => 100,
            'main_photo' => UploadedFile::fake()->image('file1.png', 400, 600),
            'smartphone_photos' => [
              0 =>  UploadedFile::fake()->image('file3.jpg'),
              1 =>  UploadedFile::fake()->image('file2.jpg'),
            ]
        ];
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('POST', '/smartphones', $smartphone, $headers)   
        ->assertStatus(500)
        ->assertJsonStructure(['message']);
           
    }

    public function testAddSmartphoneValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $smartphone = [
            'name' => 'xxadasd',
            'price' => 0.08789,
            'ram_id' => 'a',
            'memory_id' => '',
            'display_id' => 100,
            'resolution_id' => 20,
            'battery_id' => 'x',
            'camera_id' => 'select',
            'system_id' => 4,
            'processor_id' => 3,
            'count' => true,
        ];
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('POST', '/smartphones', $smartphone, $headers)   
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }
    public function testFailedAddSmartphone()
    {
        $smartphone = [
            'name' => 'Maxi 2',
            'price' => 5100.58,
            'ram_id' => 1,
            'memory_id' => 2,
            'display_id' => 1,
            'resolution_id' => 2,
            'battery_id' => 2,
            'camera_id' => 1,
            'manufacturer_id' => 3,
            'system_id' => 4,
            'processor_id' => 3,
            'count' => 100,
            'main_photo' => UploadedFile::fake()->image('file1.png', 400, 600),
        ];
        $response = $this->json('POST', '/smartphones', $smartphone, [null], ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }
}

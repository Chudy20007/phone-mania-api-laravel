<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class ManufacturerTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\ManufacturerRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetManufacturersList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/manufacturers',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }
    public function testGetManufacturersListAsGuest()
    {
        $response = $this->json('GET', '/manufacturers', ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testGetManufacturer()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/manufacturers/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetManufacturer()
    {
        $response = $this->json('GET', '/manufacturers/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeleteManufacturer()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/manufacturers/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteManufacturer()
    {
        $response = $this->json('DELETE', '/manufacturers/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateManufacturerValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $manufacturer = [
            'id' => 2,
            'name' => 'Samsung'
        ];
        $response = $this->json('PUT', '/manufacturers/2',$manufacturer, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateManufacturer()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $manufacturer = [
            'id' => 2,
            'name' => 'Meizu'
        ];
        $response = $this->json('PUT', '/manufacturers/2',$manufacturer, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedUpdateManufacturer()
    {
        $response = $this->json('PUT', '/manufacturers/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
   
    public function testAddManufacturerValidation()
    {
        $token = JWTAuth::attempt($this->credentials);        
        $manufacturer = [
            'name' => '1'
        ];
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('POST', '/manufacturers',$manufacturer, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testAddManufacturer()
    {
        $token = JWTAuth::attempt($this->credentials);        
        $manufacturer = [
            'name' => 'Meizu'
        ];
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('POST', '/manufacturers',$manufacturer, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testFailedAddManufacturer()
    {
        $manufacturer = [
            'name' => 'Meizu'
        ];
        $response = $this->json('POST', '/manufacturers',$manufacturer, [null], ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
}

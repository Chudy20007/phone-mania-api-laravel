<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class BatteryTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\BatteryRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetBatteriesList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/batteries',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }

    public function testGetBattery()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/batteries/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetBattery()
    {
        $response = $this->json('GET', '/batteries/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeleteBattery()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/batteries/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteBattery()
    {
        $response = $this->json('DELETE', '/batteries/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateBatteryValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $battery = [
            'id' => 4,
            'capacity' => 'x2500x',
            'type' => 'litowojonowa'
        ];
        $response = $this->json('PUT', '/batteries/4',$battery, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateBattery()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $battery = [
            'id' => 4,
            'capacity' => 2500,
            'type' => 'Litowo-jonowa'
        ];
        $response = $this->json('PUT', '/batteries/4',$battery, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedUpdateBattery()
    {
        $response = $this->json('PUT', '/batteries/4')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
 
    public function testAddBatteryValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $battery = [
            'capacity' => 'Litowo-jonowa',
            'type' => ''
        ];
        $response = $this->json('POST', '/batteries',$battery, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testAddBattery()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $battery = [
            'capacity' => 2500,
            'type' => 'Litowo-jonowa'
        ];
        $response = $this->json('POST', '/batteries',$battery, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testAddFailedBattery()
    {
        $battery = [
            'capacity' => 2500,
            'type' => 'Litowo-jonowa'
        ];
        $response = $this->json('POST', '/batteries',$battery,[null], ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

}

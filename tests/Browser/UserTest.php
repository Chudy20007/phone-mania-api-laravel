<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class UserTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\UserRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testSuccessfulApiLogin()
    {
        $body = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
        $response = $this->json('POST', '/login', $body, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['token_type', 'expires_in', 'access_token', 'message', 'role']);
    }
    public function testFailedApiLogin()
    {
        $body = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin2',
        ];
        $response = $this->json('POST', '/login', $body, ['Accept' => 'application/json'])
            ->assertStatus(401)
            ->assertJsonStructure(['message']);
    }
    public function testGetUsersList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/users',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }
    public function testGetFailedUsersList()
    {
        $response = $this->json('GET', '/users', ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testGetUser()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/users/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetUser()
    {
        $response = $this->json('GET', '/users/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeleteUser()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/users/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteUser()
    {
        $response = $this->json('DELETE', '/users/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateUserValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $user = [
            'id' => 2,
            'first_name' => 'xcin',
            'last_name' => 1,
            'email' => 'anonim410@gmail.com',
            'street' => '',
            'street_number' => 'x',
            'city' => '',
            'post_code' => 'A-219',
            'password' => bcrypt('test'),
        ];
        $response = $this->json('PUT', '/users/2',$user, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateUser()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $user = [
            'id' => 2,
            'first_name' => 'Marcin',
            'last_name' => 'Maziak',
            'email' => 'test@test.pl',
            'street' => 'Bliska',
            'street_number' => '15',
            'city' => 'Kielce',
            'post_code' => '21-219',
            'password' => bcrypt('test'),
        ];
        $response = $this->json('PUT', '/users/2',$user, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedUpdateUser()
    {
        $response = $this->json('PUT', '/users/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    
    public function registerUser()
    {
        $user = [
            'first_name' => 'Marcin',
            'last_name' => 'Maziak',
            'email' => 'test@test.pl',
            'street' => 'Bliska',
            'street_number' => '15',
            'city' => 'Kielce',
            'post_code' => '21-219',
            'password' => bcrypt('test'),
        ];
        $response = $this->json('POST', '/users',$user, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function registerUserValidation()
    {
        $user = [
            'first_name' => 'xcin',
            'last_name' => 1,
            'email' => 'anonim410gmail.com',
            'street' => '',
            'street_number' => 'x',
            'city' => '',
            'post_code' => 'A-219',
            'password' => bcrypt('test'),
        ];
        $response = $this->json('POST', '/users',$user, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }
}

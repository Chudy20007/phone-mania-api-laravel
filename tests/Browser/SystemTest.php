<?php

namespace Tests\Browser;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use JWTAuth;
use Tests\DuskTestCase;

class SystemTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    use DatabaseTransactions;
    protected $credentials;
    protected $repository;
    public function setUp()
    {

        parent::setUp();
        $this->beginDatabaseTransaction();
        $this->repository = $this->app->make('App\Repositories\SystemRepository');
        
        $this->credentials = [
            'email' => 'anonim410@gmail.com',
            'password' => 'admin1',
        ];
    }
    public function testGetSystemsList()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/systems',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);

    }
    public function testGetFailedSystemsList()
    {

        $response = $this->json('GET', '/systems', ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
        
    }
    public function testGetSystem()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('GET', '/systems/2',[null], $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }
    public function testFailedGetSystem()
    {
        $response = $this->json('GET', '/systems/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testDeleteSystem()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $response = $this->json('DELETE', '/systems/2',[null], $headers, ['Accept' => 'application/json'])
       ->assertStatus(200)
        ->assertJsonStructure(['message']);
    }
    public function testFailedDeleteSystem()
    {
        $response = $this->json('DELETE', '/systems/2')
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

    public function testUpdateSystem()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $system = [
            'name' => 'Android',
            'version' => '2.3 Gingerbread'
        ];
        $response = $this->json('PUT', '/systems/2',$system, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testUpdateSystemValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $system = [
            'name' => 'Android',
            'version' => '4.4.4 KitKat'
        ];
        $response = $this->json('PUT', '/systems/2',$system, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }

    public function testFailedUpdateSystem()
    {
        $system = [
            'name' => 'Android',
            'version' => '2.3 Gingerbread'
        ];
        $response = $this->json('PUT', '/systems/2',$system,[null],['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }
    public function testAddSystemValidation()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $system = [
            'name' => 0022,
            'version' => 'xadqwe213121221'
        ];
        $response = $this->json('POST', '/systems',$system, $headers, ['Accept' => 'application/json'])
        ->assertStatus(422)
        ->assertJsonStructure(['message']);
    }   
    public function testAddSystem()
    {
        $token = JWTAuth::attempt($this->credentials);
        $headers = ['Authorization' => " Bearer $token"];
        $system = [
            'name' => 'Android',
            'version' => '2.3 Gingerbread'
        ];
        $response = $this->json('POST', '/systems',$system, $headers, ['Accept' => 'application/json'])
        ->assertStatus(200)
        ->assertJsonStructure(['data']);
    }

    public function testAddFailedSystem()
    {
        $system = [
            'name' => 'Android',
            'version' => '2.3 Gingerbread'
        ];
        $response = $this->json('POST', '/systems', $system,[null], ['Accept' => 'application/json'])
        ->assertStatus(401)
        ->assertJsonStructure(['message']);
    }

}

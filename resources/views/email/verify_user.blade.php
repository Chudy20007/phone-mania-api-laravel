<div class='row text-center'>
    <div style='border-bottom:2px solid gray' class='col-md-12 col-lg-12 col-xs-12'>
<img style='width:20px; height:20px' src="{{URL::asset('/css/img/logo.png')}}"> Phone Mania</img> <br/>   Witaj <b>{{$user->first_name}} {{$user->last_name}}</b>
    </div>
<br/>
Twój zarejestrowany adres e-mail to <b>{{$user->email}}</b>.
<div class='col-md-12 col-xs-12'>
    <div>
     Proszę, kliknij w poniższy link, aby zakończyć proces rejestracji.
     <br/>
<a href="http://localhost:8080/user/verify/{{$user->verifyUser->token}}">Zweryfikuj adres e-mail</a>
    </div>
    <br/>
    <div style='border-top:2px solid gray' class='col-md-12 col-lg-12 col-xs-12'>
    Pozdrawiamy, <br/>
    Ekipa Phone Mania
    </div>
</div>

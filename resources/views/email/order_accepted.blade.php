<div class='row text-center'>
    <div style='border-bottom:2px solid gray' class='col-md-12 col-lg-12 col-xs-12'>
<img style='width:20px; height:20px' src="{{URL::asset('/css/img/logo.png')}}"> Phone Mania</img> <br/> Zmiana statusu zamówienia
    </div>
<br/>
Twoje zamówienie zmieniło status na: <b>{{$status}} </b>

<div>
Dane do faktury :
<table class="table table-light">
  <tr> <td style="padding:2px; text-align:left"> {{$user->first_name}} {{$user->last_name}} </td> </tr> </br>
  <tr><td style="padding:2px; text-align:left">  ul. {{$user->street}} {{$user->street_number}}</td></tr> </br>
   <tr><td style="padding:2px; text-align:left">  {{$user->post_code}} {{$user->city}}</td></tr>
</table>
</div>
<div class='col-md-12 col-xs-12'>
<table class="table table-light" style="padding:4px;">
        <thead style="padding:6px;
        text-align: center;
        background-color: #56647a;
        color: white;" class="thead-light">
                <tr style="padding:6px;">
                    <th style="padding:6px;"  scope="col">Produkt</th>
                     <th style="padding:6px;" scope="col">Ilość</th>        
                     <th style="padding:6px;" scope="col" >Cena jednostkowa</th>
                     <th style="padding:6px;" scope="col" >Razem</th>                    
                 </tr>
        </thead>
        <tbody>
            @foreach($order->products as $product)
            <tr style="background-color:#f2f2f2; text-align:center; padding:6px;">
            <td style="padding:6px;" scope="row">{{$product->manufacturer->name}} {{$product->name}}</td>
            <td style="padding:6px;" scope="row">{{$product->pivot->count}}</td>
            <td style="padding:6px;" scope="row">{{number_format($product->price,2)}} zł</td>
            <td style="padding:6px;" scope="row">{{number_format($product->price * $product->pivot->count,2)}} zł</td>
            </tr>
            @endforeach
            <tr style="padding:6px;">
                    <th style="padding:6px;"  scope="col">Do zapłaty</th><td style="padding:6px;" scope="row"></td><td style="padding:6px;" scope="row"></td>
                    <td style="padding:6px;" scope="row">{{number_format($order->total_price,2)}} zł</td>
        </tbody>
</table>
<br/>
<br/>
<div class='col-md-12 col-lg-12 col-xs-12'>
    <b>Sposób zapłaty :</b> {{$order->payment->name}}
</div>
</br>
    <div style='border-top:2px solid gray' class='col-md-12 col-lg-12 col-xs-12'>
    Pozdrawiamy, <br/>
    Ekipa Phone Mania
    </div>
</div>

<div class='row text-center'>
    <div style='border-bottom:2px solid gray' class='col-md-12 col-lg-12 col-xs-12'>
<img style='width:20px; height:20px' src="{{URL::asset('/css/img/logo.png')}}"> Phone Mania</img> <br/> Prośba o przypomnienie hasła
    </div>
<br/>
Otrzymaliśmy prośbę o przypomnienie hasła do twojego konta na naszej stronie internetowej.
<div class='col-md-12 col-xs-12'>

     Proszę, kliknij w poniższy link, jeżeli to ty wysłałeś do nas prośbę o zmianę hasła.
     <br/>
     <a href="{{env('APP_VUE_URL')}}/reset/{{$token}}">Zresetuj hasło</a>
    </div>
    <br/>
    <div style='border-top:2px solid gray' class='col-md-12 col-lg-12 col-xs-12'>
    Pozdrawiamy, <br/>
    Ekipa Phone Mania
    </div>
</div>
